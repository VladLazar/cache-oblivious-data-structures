function(enable_sanitizers target)
    set(SANITIZERS "address" "undefined" "leak")
    list(JOIN SANITIZERS "," FLAG_VALUE)

    target_compile_options(${target} INTERFACE -fsanitize=${FLAG_VALUE})
    target_link_libraries(${target} -fsanitize=${FLAG_VALUE})
endfunction()
