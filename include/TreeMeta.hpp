#ifndef CACHE_OBLIVIOUS_DATA_STRUCTURES_TREE_META_H
#define CACHE_OBLIVIOUS_DATA_STRUCTURES_TREE_META_H

#include <cstddef>
#include <ostream>
#include <vector>

namespace cov::implementation {

struct DepthMeta {
	bool operator==(const DepthMeta &other) const;
	bool operator!=(const DepthMeta &other) const;

	friend std::ostream &operator<<(std::ostream &os, const DepthMeta &meta);

	std::size_t bottom_tree_size;
	std::size_t top_tree_size;
	std::size_t top_tree_root_depth;
};

class TreeMeta {
public:
	explicit TreeMeta(const std::size_t &height);

	bool operator==(const TreeMeta &other) const;
	bool operator!=(const TreeMeta &other) const;

	[[nodiscard]] inline const DepthMeta &operator[](const std::size_t &index) const {
		return meta[index];
	}


	[[nodiscard]] std::size_t tree_height() const;

private:
	[[nodiscard]] std::vector<DepthMeta> compute_meta(const std::size_t &max_depth);
	[[nodiscard]] std::size_t size_of_tree(const std::size_t &max_depth) const;

private:
	std::vector<DepthMeta> meta;
};

} // namespace cov::implementation

namespace std {
template <>
struct hash<cov::implementation::TreeMeta> {
	std::size_t operator()(const cov::implementation::TreeMeta &meta) const noexcept {
		return std::hash<std::size_t>{}(meta.tree_height());
	}
};
} // namespace std

#endif // CACHE_OBLIVIOUS_DATA_STRUCTURES_TREE_META_H
