#pragma once

#include <any>
#include <iostream>
#include <optional>
#include <type_traits>

namespace cov::implementation {

template <typename T, typename TT>
struct is_optional_of : std::false_type {};

template <typename T>
struct is_optional_of<T, std::optional<T>> : std::true_type {};

template <typename T>
struct is_optional_of<std::any, std::optional<T>> : std::true_type {};

static constexpr inline bool is_left_child(std::size_t bfs_index) {
	return bfs_index % 2 == 0;
}

template <typename ForwardIt>
[[nodiscard]] static constexpr bool is_sorted_with_gaps(ForwardIt first, ForwardIt last) {
	ForwardIt last_value = last;
	for (ForwardIt next = first; next != last; ++next) {
		if (next->has_value()) {
			if (last_value != last and last_value->value() > next->value()) {
				return false;
			}

			last_value = next;
		}
	}

	return true;
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const std::optional<T> &optional) {
	if (optional.has_value()) {
		os << optional.value();
	} else {
		os << "nullopt";
	}

	return os;
}

} // namespace cov::implementation
