#ifndef CACHE_OBLIVIOUS_DATA_STRUCTURES_TREE_META_MANAGER_H
#define CACHE_OBLIVIOUS_DATA_STRUCTURES_TREE_META_MANAGER_H

#include "TreeMeta.hpp"

#include <unordered_map>

namespace cov::implementation {

class TreeMetaManager {
public:
	const TreeMeta &get_tree_meta(const std::size_t &height);

private:
	std::unordered_map<std::size_t, TreeMeta> tree_meta_cache;
};

} // namespace cov::implementation

#endif // CACHE_OBLIVIOUS_DATA_STRUCTURES_TREE_META_MANAGER_H
