#pragma once

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <functional>
#include <iostream>
#include <iterator>
#include <optional>
#include <vector>

#include "assert_config.hpp"
#include "debug_assert.hpp"
#include "mtr/metrics.hpp"

namespace cov {

struct packed_memory_array_config {
	// double rho_leaf = 1.0 / 4;
	// double rho_root = 1.0 / 2;

	// double tau_root = 3.0 / 4;
	// double tau_leaf = 1.0;

	double rho_leaf = 0.1;
	double rho_root = 0.3;

	double tau_leaf = 1;
	double tau_root = 0.75;

	double c = 1.35;
};

template <typename T>
class packed_memory_array {
public:
	using value_type = T;
	using iterator = typename std::vector<std::optional<T>>::iterator;
	using const_iterator = typename std::vector<std::optional<T>>::const_iterator;


	static constexpr size_t DEFAULT_CAPACITY = 2;

	explicit packed_memory_array(const packed_memory_array_config &config = {});


	template <typename InputIt>
	explicit packed_memory_array(InputIt begin,
	                             InputIt end,
	                             const packed_memory_array_config &config = {});

	const_iterator find(const T &value) const;
	std::pair<iterator, iterator> insert(std::size_t index, const T &value);
	std::pair<iterator, iterator> remove(std::size_t index);

	iterator begin() noexcept;
	const_iterator begin() const noexcept;
	const_iterator cbegin() const noexcept;

	iterator end() noexcept;
	const_iterator end() const noexcept;
	const_iterator cend() const noexcept;

	[[nodiscard]] std::size_t size() const;
	[[nodiscard]] std::size_t capacity() const;

	const std::vector<std::optional<T>> &data() const;

private:
	explicit packed_memory_array(std::size_t capacity,
	                             const packed_memory_array_config &config = {});

	void shrink();
	void grow_with(const T &value);
	void resize(const std::optional<T> &extra = std::nullopt);
	std::pair<iterator, iterator> rebalance(std::size_t target_segment,
	                                        const std::optional<T> &value = std::nullopt);

	void redistribute(iterator begin,
	                  iterator end,
	                  const std::optional<T> &extra = std::nullopt);

	template <typename InputIt>
	void distribute(iterator dest_begin,
	                iterator dest_end,
	                InputIt source_begin,
	                InputIt source_end);

	template <typename It>
	void reset_range_metadata(It begin, It end);

	[[nodiscard]] inline double rho(std::size_t depth) const;
	[[nodiscard]] inline double tau(std::size_t depth) const;
	[[nodiscard]] std::vector<std::uint16_t> initialise_occupied() const;

	[[nodiscard]] std::pair<iterator, iterator> find_rebalancing_interval(
	    std::size_t segment,
	    const std::function<bool(std::size_t, std::size_t, std::size_t)> &pred);

	[[nodiscard]] static inline std::size_t bfs_peer(std::size_t index);
	[[nodiscard]] static inline std::size_t get_capacity(std::size_t elements,
	                                                     double extra_capacity);

	template <typename InputIt, typename OutputIt>
	static inline void unwrap_optional_range(InputIt begin, InputIt end, OutputIt out);

private:
	packed_memory_array_config config_;
	std::size_t occupied_ = 0;

	std::size_t capacity_ = DEFAULT_CAPACITY;
	std::vector<std::optional<T>> data_;

	std::vector<std::uint16_t> occupied_in_segment_;
	std::size_t implicit_tree_depth_;
	std::size_t segment_size_;
};

template <typename T>
inline packed_memory_array<T>::packed_memory_array(
    const packed_memory_array_config &config)
    : config_(config)
    , data_(capacity_)
    , occupied_in_segment_(initialise_occupied())
    , implicit_tree_depth_(std::log2(occupied_in_segment_.size()))
    , segment_size_(capacity_ / occupied_in_segment_.size()) {}

template <typename T>
inline packed_memory_array<T>::packed_memory_array(
    std::size_t capacity,
    const packed_memory_array_config &config)
    : config_(config)
    , capacity_(capacity)
    , data_(capacity_)
    , occupied_in_segment_(initialise_occupied())
    , implicit_tree_depth_(std::log2(occupied_in_segment_.size()))
    , segment_size_(capacity_ / occupied_in_segment_.size()) {}

template <typename T>
template <typename InputIt>
inline packed_memory_array<T>::packed_memory_array(
    InputIt begin,
    InputIt end,
    const packed_memory_array_config &config)
    : packed_memory_array(get_capacity(std::distance(begin, end), config.c), config) {
	if (not std::is_sorted(begin, end)) {
		std::sort(begin, end);
	}

	occupied_ = std::distance(begin, end);
	distribute(data_.begin(), data_.end(), begin, end);
}

template <typename T>
inline auto packed_memory_array<T>::find(const T &value) const -> const_iterator {
	return std::find(data_.cbegin(), data_.cend(), value);
}

template <typename T>
inline std::pair<typename packed_memory_array<T>::iterator,
                 typename packed_memory_array<T>::iterator>
packed_memory_array<T>::insert(std::size_t index, const T &value) {
	/* If the array needs to grow, insert the value during the process. */
	if (capacity_ < ++occupied_ * config_.c) {
		grow_with(value);

		/*
		 * All of the underlying data may have been moved after a grow,
		 * so the entire range is returned.
		 */
		return {data_.begin(), data_.end()};
	}

	const std::size_t segment = index / segment_size_;
	/* If the current segment can house an extra element, redistribute within said
	 * segment. */
	if (occupied_in_segment_[segment] < segment_size_) {
		METRICS_RECORD_BLOCK("packed_memory_array::trivial_insert");

		const auto first = data_.begin() + segment * segment_size_;
		const auto last = data_.begin() + (segment + 1) * segment_size_;

		redistribute(first, last, value);

		return {first, last};
	}

	/* Otherwise, insert by rebalancing.*/
	return rebalance(segment, value);
}

template <typename T>
inline std::pair<typename packed_memory_array<T>::iterator,
                 typename packed_memory_array<T>::iterator>

packed_memory_array<T>::remove(std::size_t index) {
	DEBUG_ASSERT(data_[index].has_value(), assert_config{},
	             "No data to delete at the desired index");

	data_[index].reset();

	const std::size_t segment = index / segment_size_;
	--occupied_in_segment_[segment];

	if (--occupied_ * config_.c < capacity_ / 2 and occupied_ > 0) {
		shrink();
		return {data_.begin(), data_.end()};
	}

	return {data_.begin() + index, data_.begin() + index + 1};
}

template <typename T>
inline auto packed_memory_array<T>::begin() noexcept -> iterator {
	return data_.begin();
}

template <typename t>
inline auto packed_memory_array<t>::begin() const noexcept -> const_iterator {
	return data_.begin();
}

template <typename t>
inline auto packed_memory_array<t>::cbegin() const noexcept -> const_iterator {
	return data_.cbegin();
}

template <typename T>
inline auto packed_memory_array<T>::end() noexcept -> iterator {
	return data_.end();
}

template <typename T>
inline auto packed_memory_array<T>::end() const noexcept -> const_iterator {
	return data_.end();
}

template <typename T>
inline auto packed_memory_array<T>::cend() const noexcept -> const_iterator {
	return data_.cend();
}

template <typename T>
inline std::pair<typename packed_memory_array<T>::iterator,
                 typename packed_memory_array<T>::iterator>
packed_memory_array<T>::rebalance(std::size_t target_segment,
                                  const std::optional<T> &value) {
	METRICS_RECORD_BLOCK("packed_memory_array::rebalance");

	const auto pred = [&](std::size_t occ, std::size_t l, std::size_t r) {
		const double density =
		    static_cast<double>(occ + 1) / ((r - l + 1) * segment_size_);

		const std::size_t depth = implicit_tree_depth_ - std::log2(r - l + 1);
		return rho(depth) <= density and density <= tau(depth);
	};

	const auto [begin, end] = find_rebalancing_interval(target_segment, pred);
	redistribute(begin, end, value);

	// std::cout << std::distance(begin, end) << "/" << capacity_ << std::endl;

	return {begin, end};
}

template <typename T>
inline double packed_memory_array<T>::rho(std::size_t depth) const {
	return config_.rho_root -
	       (config_.rho_root - config_.rho_leaf) / implicit_tree_depth_ * depth;
}

template <typename T>
inline double packed_memory_array<T>::tau(std::size_t depth) const {
	return config_.tau_root +
	       (config_.tau_leaf - config_.tau_root) / implicit_tree_depth_ * depth;
}

template <typename T>
inline std::size_t packed_memory_array<T>::bfs_peer(std::size_t index) {
	/*
	 * Compute the peer of a node in the implicit tree.
	 * The result of computing the peer for the root is undefined.
	 */
	return (index & 1U) != 0U ? index & ~1U : index | 1U;
}

template <typename T>
inline std::size_t packed_memory_array<T>::get_capacity(std::size_t elements,
                                                        double extra_capacity) {
	return std::pow(2, std::ceil(std::log2(elements * extra_capacity)));
}

template <typename T>
inline void packed_memory_array<T>::redistribute(iterator begin,
                                                 iterator end,
                                                 const std::optional<T> &extra) {
	std::vector<T> values_in_interval;
	values_in_interval.reserve(std::distance(begin, end));

	unwrap_optional_range(begin, end, std::back_inserter(values_in_interval));
	reset_range_metadata(begin, end);
	if (extra.has_value()) {
		const auto position = std::upper_bound(values_in_interval.begin(),
		                                       values_in_interval.end(), *extra);
		values_in_interval.insert(position, *extra);
	}

	distribute(begin, end, values_in_interval.begin(), values_in_interval.end());
}

template <typename T>
template <typename InputIt>
inline void packed_memory_array<T>::distribute(iterator dest_begin,
                                               iterator dest_end,
                                               InputIt source_begin,
                                               InputIt source_end) {
	const auto destination_size = std::distance(dest_begin, dest_end);
	const auto source_size = std::distance(source_begin, source_end);

	const auto empty_slots = destination_size - source_size;
	const auto gap = empty_slots / (source_size);

	for (auto iter = source_begin; iter != source_end; ++iter) {
		const auto segment = std::distance(data_.begin(), dest_begin) / segment_size_;
		++occupied_in_segment_[segment];

		*dest_begin = *iter;
		std::advance(dest_begin, gap + 1);
	}

	DEBUG_ASSERT(dest_begin <= dest_end, assert_config{},
	             "The begin iterator crossed over the end one");
}

template <typename T>
inline void packed_memory_array<T>::resize(const std::optional<T> &extra) {
	std::vector<T> values;
	values.reserve(occupied_);

	unwrap_optional_range(data_.begin(), data_.end(), std::back_inserter(values));

	if (extra.has_value()) {
		values.insert(std::upper_bound(values.begin(), values.end(), *extra), *extra);
	}

	*this = packed_memory_array(values.begin(), values.end());
}

template <typename T>
inline void packed_memory_array<T>::shrink() {
	DEBUG_ASSERT(--occupied_ * config_.c < capacity_ / 2, assert_config{},
	             "Too few elements stored to grow");

	resize();
}

template <typename T>
inline void packed_memory_array<T>::grow_with(const T &new_value) {
	DEBUG_ASSERT(capacity_ < occupied_ * config_.c, assert_config{},
	             "Too few elements stored to grow");

	resize(new_value);
}

template <typename T>
inline const std::vector<std::optional<T>> &packed_memory_array<T>::data() const {
	return data_;
}

template <typename T>
inline std::size_t packed_memory_array<T>::size() const {
	return occupied_;
}

template <typename T>
inline std::size_t packed_memory_array<T>::capacity() const {
	return capacity_;
}

template <typename T>
inline std::pair<typename packed_memory_array<T>::iterator,
                 typename packed_memory_array<T>::iterator>
packed_memory_array<T>::find_rebalancing_interval(
    std::size_t segment,
    const std::function<bool(std::size_t, std::size_t, std::size_t)> &pred) {

	/* Indices of the leftmost and rightmost segments in the interval */
	auto left = segment;
	auto right = segment;

	std::size_t segments_in_interval = 1;
	std::size_t occupied = occupied_in_segment_[segment];

	/*
	 * The BFS index of the root of the tree that has
	 * all the segments required for rebalancing as leaves
	 */
	std::size_t implicit_root = std::pow(2, implicit_tree_depth_) + segment;

	/*
	 * The current segment always doubles in size by extending
	 * left or right. In order to determine the direction we look
	 * at the BFS index of the root's peer.
	 */
	while (implicit_root != 1 and not pred(occupied, left, right)) {
		const auto root_peer = bfs_peer(implicit_root);
		if (root_peer < implicit_root) {
			for (auto i = left - segments_in_interval; i <= left - 1; ++i) {
				occupied += occupied_in_segment_[i];
			}

			left -= segments_in_interval;
		} else if (root_peer > implicit_root) {
			for (auto i = right + 1; i <= right + segments_in_interval; ++i) {
				occupied += occupied_in_segment_[i];
			}

			right += segments_in_interval;
		}

		segments_in_interval *= 2;
		implicit_root /= 2;
	}

	DEBUG_ASSERT(left >= 0 and right < occupied_in_segment_.size(), assert_config{},
	             "The rebalancing segment is not in bounds");

	return {data_.begin() + left * segment_size_,
	        data_.begin() + (right + 1) * segment_size_};
}

template <typename T>
inline std::vector<std::uint16_t> packed_memory_array<T>::initialise_occupied() const {
	/*
	 * The number of segments needs to be a power of two. This is
	 * unlikely to happen if we compute it by: segments / log2(segments).
	 * Let us call that the ideal number of segments and compute the
	 * largest power of two that is smaller than it. That gives the number
	 * of segments which will be actually used.
	 */
	const std::size_t ideal_segments = capacity_ / std::log2(capacity_);
	const std::size_t segments = std::pow(2, std::floor(std::log2(ideal_segments)));
	const std::size_t segment_size_pre = capacity_ / segments;

	DEBUG_ASSERT(segment_size_pre * segments == capacity_, assert_config{},
	             "The segment size is not a divisor of capacity");

	return std::vector<std::uint16_t>(segments, 0);
}

template <typename T>
template <typename It>
void packed_memory_array<T>::reset_range_metadata(It begin, It end) {
	const auto first_segment = std::distance(data_.begin(), begin) / segment_size_;
	const auto last_segment = std::distance(data_.begin(), end) / segment_size_;

	for (auto i = first_segment; i < last_segment; ++i) {
		occupied_in_segment_[i] = 0;
	}
}

template <typename T>
template <typename InputIt, typename OutputIt>
inline void packed_memory_array<T>::unwrap_optional_range(InputIt begin,
                                                          InputIt end,
                                                          OutputIt out) {
	for (auto iter = begin; iter != end; ++iter) {
		if (iter->has_value()) {
			*out++ = iter->value();
			iter->reset();
		}
	}
}

} // namespace cov
