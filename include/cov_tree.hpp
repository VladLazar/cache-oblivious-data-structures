/* TODO: * Implement const_cov_tree_iterator, then make make_iterator() const */

#pragma once

#include <cstddef>
#include <iterator>
#include <optional>
#include <stack>
#include <type_traits>
#include <variant>
#include <vector>

#include "assert_config.hpp"
#include "debug_assert.hpp"
#include "mtr/metrics.hpp"
#include "packed_memory_array.hpp"
#include "util.hpp"
#include "veb_index.hpp"

namespace cov {

template <typename T, typename Container>
struct node;

template <typename T, typename Container>
class cov_tree_iterator;

template <typename T,
          typename Container = packed_memory_array<T>,
          typename Index = implementation::veb_index<T, typename Container::iterator>>
class cov_tree {
public:
	using iterator = cov_tree_iterator<T, Container>;

	explicit cov_tree();

	std::pair<iterator, bool> insert(const T &value);
	iterator find(const T &value);
	iterator erase(iterator pos);

	iterator begin() noexcept;
	iterator end() noexcept;

private:
	inline iterator make_iterator(typename Container::iterator container_iterator);

private:
	Container data_;
	Index index_;
};

template <typename T, typename Container>
class cov_tree_iterator {
	static_assert(
	    implementation::is_optional_of<T,
	                                   typename Container::iterator::value_type>::value);

	using container_iterator = typename Container::iterator;

public:
	using value_type = T;
	using difference_type = std::ptrdiff_t;
	using reference = value_type &;
	using pointer = value_type *;
	using iterator_category = std::bidirectional_iterator_tag;

	cov_tree_iterator() noexcept;

	reference operator*() const;
	pointer operator->() const;

	cov_tree_iterator &operator++();
	cov_tree_iterator operator++(int);

	cov_tree_iterator &operator--();
	cov_tree_iterator operator--(int);

	template <typename A, typename B>
	friend bool operator==(const cov_tree_iterator<A, B> &x,
	                       const cov_tree_iterator<A, B> &y);

	template <typename A, typename B>
	friend bool operator!=(const cov_tree_iterator<A, B> &x,
	                       const cov_tree_iterator<A, B> &y);

private:
	explicit cov_tree_iterator(container_iterator iter,
	                           container_iterator first,
	                           container_iterator last) noexcept;
	[[nodiscard]] inline std::size_t index() const;

private:
	container_iterator container_iterator_;
	container_iterator first_;
	container_iterator last_;

private:
	template <class, class, class>
	friend class cov_tree;
};

template <typename T, typename Container, typename Index>
inline cov_tree<T, Container, Index>::cov_tree()
    : data_(), index_(data_.begin(), data_.end()) {}

template <typename T, typename Container, typename Index>
inline std::pair<typename cov_tree<T, Container, Index>::iterator, bool>
cov_tree<T, Container, Index>::insert(const T &value) {
	METRICS_RECORD_BLOCK("cov_tree::insert");

	const typename Container::iterator iter = index_.find(value);
	if (*iter == value) {
		return {make_iterator(iter), false};
	}


	const auto [first, last] = data_.insert(std::distance(data_.begin(), iter), value);
	if (first == data_.begin() and last == data_.end() and
	    index_.size() != static_cast<std::size_t>(std::distance(first, last))) {
		/* If the underlying container grew, the index needs to be rebuilt and the value
		 * seeked for. */
		DEBUG_ASSERT(implementation::is_sorted_with_gaps(data_.begin(), data_.end()),
		             assert_config{},
		             "Underlying container is not sorted before index rebuild");
		index_ = Index(first, last);
		return {find(value), true};
	}

	const typename Container::iterator inserted_at =
	    index_.update(first, last, [&value](auto &&v) { return v == value; });

	DEBUG_ASSERT(implementation::is_sorted_with_gaps(data_.begin(), data_.end()),
	             assert_config{}, "Underlying container is not sorted");

	return {make_iterator(inserted_at), true};
}

template <typename T, typename Container, typename Index>
inline typename cov::cov_tree<T, Container, Index>::iterator
cov_tree<T, Container, Index>::erase(
    typename cov_tree<T, Container, Index>::iterator pos) {
	const auto value = *pos;

	const auto [first, last] = data_.remove(pos.index());

	if (first == data_.begin() and last == data_.end() and
	    index_.size() != static_cast<std::size_t>(std::distance(first, last))) {
		index_ = Index(first, last);
		return find(value);
	}

	typename Container::iterator after_erased =
	    index_.update(first, last, [&value](auto &&v) { return v > value; });

	/* If the largest element in the rebalanced segment was erased,
	 * we need to iterate forward in the container until we find the next value. */
	while (after_erased != data_.end() and not after_erased->has_value()) {
		++after_erased;
	}

	return make_iterator(after_erased);
}

template <typename T, typename Container, typename Index>
inline typename cov::cov_tree<T, Container, Index>::iterator
cov_tree<T, Container, Index>::find(const T &value) {
	const typename Container::iterator container_iter = index_.find(value);
	return *container_iter == value ? make_iterator(container_iter) : end();
}

template <typename T, typename Container, typename Index>
inline typename cov::cov_tree<T, Container, Index>::iterator
cov_tree<T, Container, Index>::begin() noexcept {
	typename Container::iterator iter = data_.begin();
	while (not iter->has_value() and iter != data_.end()) {
		++iter;
	}

	return make_iterator(iter);
}

template <typename T, typename Container, typename Index>
inline typename cov::cov_tree<T, Container, Index>::iterator
cov_tree<T, Container, Index>::end() noexcept {
	return make_iterator(data_.end());
}

template <typename T, typename Container, typename Index>
inline typename cov::cov_tree<T, Container, Index>::iterator
cov_tree<T, Container, Index>::make_iterator(
    typename Container::iterator container_iterator) {
	DEBUG_ASSERT(container_iterator == data_.end() or container_iterator->has_value(),
	             assert_config{},
	             "Call to make_iterator with container iterator to std::nullopt");

	return cov_tree_iterator<T, Container>(container_iterator, data_.begin(),
	                                       data_.end());
}

template <typename T, typename Container>
inline cov_tree_iterator<T, Container>::cov_tree_iterator() noexcept = default;

template <typename T, typename Container>
inline cov_tree_iterator<T, Container>::cov_tree_iterator(
    typename cov_tree_iterator<T, Container>::container_iterator iter,
    typename cov_tree_iterator<T, Container>::container_iterator first,
    typename cov_tree_iterator<T, Container>::container_iterator last) noexcept
    : container_iterator_(iter), first_(first), last_(last) {}

template <typename T, typename Container>
inline typename cov_tree_iterator<T, Container>::reference
    cov_tree_iterator<T, Container>::operator*() const {
	return container_iterator_->value();
}

template <typename T, typename Container>
inline typename cov_tree_iterator<T, Container>::pointer cov_tree_iterator<T, Container>::
operator->() const {
	return &(container_iterator_->value());
}

template <typename T, typename Container>
inline cov_tree_iterator<T, Container> &cov_tree_iterator<T, Container>::operator++() {
	do {
		++container_iterator_;
	} while (container_iterator_ != last_ and not container_iterator_->has_value());

	return *this;
}

template <typename T, typename Container>
inline cov_tree_iterator<T, Container> cov_tree_iterator<T, Container>::operator++(int) {
	cov_tree_iterator tmp(*this);
	++(*this);
	return tmp;
}

template <typename T, typename Container>
inline cov_tree_iterator<T, Container> &cov_tree_iterator<T, Container>::operator--() {
	do {
		--container_iterator_;
	} while (container_iterator_ >= first_ and not container_iterator_->has_value());

	return *this;
}

template <typename T, typename Container>
inline cov_tree_iterator<T, Container> cov_tree_iterator<T, Container>::operator--(int) {
	cov_tree_iterator tmp(*this);
	--(*this);
	return tmp;
}

template <typename T, typename Container>
inline bool operator==(const cov_tree_iterator<T, Container> &x,
                       const cov_tree_iterator<T, Container> &y) {
	return x.container_iterator_ == y.container_iterator_;
}

template <typename T, typename Container>
inline bool operator!=(const cov_tree_iterator<T, Container> &x,
                       const cov_tree_iterator<T, Container> &y) {
	return !(x == y);
}

template <typename T, typename Container>
inline std::size_t cov_tree_iterator<T, Container>::index() const {
	return std::distance(first_, container_iterator_);
}


} // namespace cov
