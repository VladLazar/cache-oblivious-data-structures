#pragma once

#include <algorithm>
#include <cmath>
#include <memory>
#include <type_traits>
#include <vector>

#include "assert_config.hpp"
#include "debug_assert.hpp"

namespace cov {


struct find_strategy_branchy {};
struct find_strategy_branch_free {};


template <typename T, typename find_strategy = find_strategy_branchy>
class static_bfs_tree {
public:
	static constexpr std::size_t npos = -1;

	explicit static_bfs_tree();

	template <typename InputIt>
	explicit static_bfs_tree(InputIt first, InputIt last);

	[[nodiscard]] std::size_t size() const;
	[[nodiscard]] std::size_t find(const T &value) const;

	const T &operator[](std::size_t idx) const;

private:
	[[nodiscard]] std::size_t find_branchy(const T &value) const;
	[[nodiscard]] std::size_t find_branch_free(const T &value) const;

	template <typename InputIt>
	void populate(InputIt iter);

	template <typename InputIt>
	InputIt populate_impl(InputIt iter, std::size_t index);

private:
	std::size_t size_ = 0;
	std::size_t depth_ = 0;
	std::unique_ptr<T[]> data_;
};

template <typename T, typename find_strategy>
inline static_bfs_tree<T, find_strategy>::static_bfs_tree() = default;

template <typename T, typename find_strategy>
template <typename InputIt>
inline static_bfs_tree<T, find_strategy>::static_bfs_tree(InputIt first, InputIt last)
    : size_(std::distance(first, last))
    , depth_(std::floor(std::log2(size_)))
    , data_(std::make_unique<T[]>(size_)) {
	if (std::is_sorted(first, last)) {
		populate(first);
		return;
	}

	std::vector<T> copy_of_range(first, last);
	std::sort(copy_of_range.begin(), copy_of_range.end());

	populate(copy_of_range.begin());
}

template <typename T, typename find_strategy>
inline std::size_t static_bfs_tree<T, find_strategy>::size() const {
	return size_;
}

template <typename T, typename find_strategy>
inline auto static_bfs_tree<T, find_strategy>::find(const T &value) const -> std::size_t {
	if constexpr (std::is_same_v<find_strategy, find_strategy_branchy>) {
		return find_branchy(value);
	} else {
		return find_branch_free(value);
	}
}

template <typename T, typename find_strategy>
inline auto static_bfs_tree<T, find_strategy>::operator[](std::size_t idx) const
    -> const T & {
	return data_[idx];
}

template <typename T, typename find_strategy>
inline auto static_bfs_tree<T, find_strategy>::find_branchy(const T &value) const
    -> std::size_t {
	std::size_t current = 1;
	while (current - 1 < size_) {
		const auto &node = data_[current - 1];

		if (value < node) {
			current = current * 2;
		} else if (value > node) {
			current = current * 2 + 1;
		} else if (node == value) {
			return current - 1;
		}
	}

	return npos;
}

template <typename T, typename find_strategy>
inline auto static_bfs_tree<T, find_strategy>::find_branch_free(const T &value) const
    -> std::size_t {
	std::size_t current = 1;
	while (current - 1 < size_) {
		const auto &node = data_[current - 1];
		if (node == value) {
			return current - 1;
		}

		current = value < node ? current * 2 : current * 2 + 1;
	}

	return npos;
}

template <typename T, typename find_strategy>
template <typename InputIt>
inline void static_bfs_tree<T, find_strategy>::populate(InputIt iter) {
	populate_impl(iter, 0);
}

template <typename T, typename find_strategy>
template <typename InputIt>
inline InputIt static_bfs_tree<T, find_strategy>::populate_impl(InputIt iter,
                                                                std::size_t index) {
	if (index >= size_) {
		return iter;
	}

	iter = populate_impl(iter, 2 * index + 1);

	data_[index] = *iter;
	++iter;

	iter = populate_impl(iter, 2 * index + 2);

	return iter;
}

} // namespace cov
