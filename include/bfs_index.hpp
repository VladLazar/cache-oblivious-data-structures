#pragma once

#include <cmath>
#include <cstdint>
#include <iostream>
#include <optional>
#include <variant>
#include <vector>

#include "mtr/metrics.hpp"

#include "assert_config.hpp"
#include "debug_assert.hpp"
#include "util.hpp"

namespace cov::implementation {

template <typename T, typename Iter>
class bfs_node;

template <typename T, typename Iter>
class bfs_index {
public:
	explicit bfs_index(Iter first, Iter last);

	Iter find(const T &value) const;

	template <typename Pred>
	Iter update(Iter first, Iter last, Pred &&pred);

	[[nodiscard]] inline const std::vector<bfs_node<T, Iter>> &data() const;
	[[nodiscard]] inline std::size_t size() const;

private:
	void populate_index();
	std::optional<T> populate_index_implementation(std::size_t root, std::uint16_t depth);

private:
	Iter first_;
	Iter last_;

	std::size_t tree_size_;
	std::size_t max_depth_;
	std::size_t leaves_offset_;

	std::vector<bfs_node<T, Iter>> tree_;
};

template <typename T, typename Iter>
class bfs_node {
public:
	using value_type = std::variant<std::optional<T>, Iter>;

	bfs_node() = default;

	bfs_node(value_type value, std::size_t index);

	[[nodiscard]] std::size_t left_child_index() const;
	[[nodiscard]] std::size_t parent_index() const;
	[[nodiscard]] std::optional<T> value() const;
	[[nodiscard]] const value_type &raw_value() const;

	template <typename E>
	void set_value(const E &value);
	void set_value(const value_type &value);

	constexpr bool operator==(const bfs_node &other) const;
	constexpr bool operator<(const bfs_node &other) const;

private:
	value_type value_;
	std::size_t index_;
};

template <typename T, typename Iter>
std::ostream &operator<<(std::ostream &os, const bfs_node<T, Iter> &node);

template <typename T, typename Iter>
inline bfs_index<T, Iter>::bfs_index(Iter first, Iter last)
    : first_(first)
    , last_(last)
    , tree_size_((2 * std::distance(first_, last_)) - 1)
    , max_depth_(std::log2(tree_size_))
    , leaves_offset_(std::pow(2, max_depth_))
    , tree_(tree_size_) {
	populate_index();
}

template <typename T, typename Iter>
inline Iter bfs_index<T, Iter>::find(const T &value) const {
	METRICS_RECORD_BLOCK("bfs_index::find");

	std::size_t current_index = 1;
	for (std::size_t depth = 0; depth < max_depth_; ++depth) {
		DEBUG_ASSERT(std::holds_alternative<std::optional<T>>(
		                 tree_[current_index - 1].raw_value()),
		             assert_config{}, "Leaf node inspected during search");

		const auto left = current_index * 2;
		const auto right = current_index * 2 + 1;

		bool go_left = tree_[left - 1].value() >= value;
		current_index = go_left ? left : right;
	}

	DEBUG_ASSERT(std::holds_alternative<Iter>(tree_[current_index - 1].raw_value()),
	             assert_config{}, "Search result is a non leaf node");

	return std::get<Iter>(tree_[current_index - 1].raw_value());
}

template <typename T, typename Iter>
template <typename Pred>
inline Iter bfs_index<T, Iter>::update(Iter first, Iter last, Pred &&pred) {
	if (std::distance(first_, first) % 2 == 1) {
		--first;
	}

	if (std::distance(first_, last) % 2 == 1) {
		++last;
	}

	auto found = last;
	for (auto iter = first; iter != last; iter += 2) {
		/* Check if any of the two peer leaves satisfy the predcate. */
		if (found == last) {
			if (pred(*iter)) {
				found = iter;
			} else if (pred(*(iter + 1))) {
				found = iter + 1;
			}
		}

		std::optional<T> local_max = std::max(*iter, *(iter + 1));
		std::size_t current_index = leaves_offset_ + std::distance(first_, iter + 1);

		DEBUG_ASSERT(current_index % 2 == 1, assert_config{},
		             "Propagation must start from a right child");

		while (not is_left_child(current_index) and current_index != 1) {
			const auto parent = tree_[current_index - 1].parent_index();

			/* If propagating null check if the left child has a value. If it does
			 * 'pick it up' and propagate it. */
			if (local_max == std::nullopt) {
				const auto peer = tree_[parent - 1].left_child_index();
				local_max = tree_[peer - 1].value();
			}

			tree_[parent - 1].set_value(local_max);
			current_index = parent;
		}
	}

	return found;
}

template <typename T, typename Iter>
inline const std::vector<bfs_node<T, Iter>> &bfs_index<T, Iter>::data() const {
	return tree_;
}

template <typename T, typename Iter>
inline std::size_t bfs_index<T, Iter>::size() const {
	return std::distance(first_, last_);
}

template <typename T, typename Iter>
inline void bfs_index<T, Iter>::populate_index() {
	populate_index_implementation(1, 0);
}

template <typename T, typename Iter>
inline std::optional<T> bfs_index<T, Iter>::populate_index_implementation(
    std::size_t root,
    std::uint16_t depth) {
	/* If on a leaf node, update tree_ and then bail. */
	if (depth == max_depth_) {
		Iter range_iter = first_ + (root - leaves_offset_);
		tree_[root - 1] = bfs_node<T, Iter>(range_iter, root);

		return *range_iter;
	}

	/* Otherwise, visit the children. */
	const auto left = populate_index_implementation(root * 2, depth + 1);
	const auto right = populate_index_implementation(root * 2 + 1, depth + 1);

	if (right.has_value()) {
		DEBUG_ASSERT(left < right, assert_config{}, "Max was found in the left subtree");
	}

	tree_[root - 1] = bfs_node<T, Iter>(std::max(left, right), root);
	return std::max(left, right);
}

template <typename T, typename Iter>
inline bfs_node<T, Iter>::bfs_node(value_type value, std::size_t index)
    : value_(std::move(value)), index_(index) {}

template <typename T, typename Iter>
std::size_t bfs_node<T, Iter>::left_child_index() const {
	return index_ * 2;
}

template <typename T, typename Iter>
std::size_t bfs_node<T, Iter>::parent_index() const {
	return index_ / 2;
}

template <typename T, typename Iter>
std::optional<T> bfs_node<T, Iter>::value() const {
	return std::visit(
	    [](auto &&v) -> std::optional<T> {
		    using E = std::decay_t<decltype(v)>;
		    if constexpr (std::is_same_v<E, Iter>) {
			    return *v;
		    } else {
			    return v;
		    }
	    },
	    value_);
}

template <typename T, typename Iter>
auto bfs_node<T, Iter>::raw_value() const -> const value_type & {
	return value_;
}

template <typename T, typename Iter>
template <typename E>
void bfs_node<T, Iter>::set_value(const E &value) {
	value_ = value;
}

template <typename T, typename Iter>
void bfs_node<T, Iter>::set_value(const value_type &value) {
	if (std::holds_alternative<Iter>(value)) {
		value_ = *std::get<Iter>(value);
	} else {
		value_ = std::get<std::optional<T>>(value);
	}
}

template <typename T, typename Iter>
constexpr bool bfs_node<T, Iter>::operator==(const bfs_node &other) const {
	if (index_ != other.index_) {
		return false;
	}

	/* Special case the compare the values pointed to */
	if (std::holds_alternative<Iter>(value_) and
	    std::holds_alternative<Iter>(other.value_)) {
		return *(std::get<Iter>(value_)) == *(std::get<Iter>(other.value_));
	}

	return value_ == other.value_;
}

template <typename T, typename Iter>
constexpr bool bfs_node<T, Iter>::operator<(const bfs_node &other) const {
	return value() < other.value();
}

template <typename T, typename Iter>
std::ostream &operator<<(std::ostream &os, const bfs_node<T, Iter> &node) {
	if (std::holds_alternative<Iter>(node.raw_value())) {
		os << "[Iterator to: " << *(std::get<Iter>(node.raw_value())) << "]";
	} else {
		os << "[Optional: " << std::get<std::optional<T>>(node.raw_value()) << "]";
	}

	return os;
}

} // namespace cov::implementation
