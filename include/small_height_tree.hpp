#pragma once

#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstddef>
#include <functional>
#include <iostream>
#include <iterator>
#include <optional>
#include <queue>
#include <stack>
#include <vector>

#include "TreeMetaManager.hpp"
#include "assert_config.hpp"
#include "debug_assert.hpp"
#include "mtr/metrics.hpp"

namespace cov {

template <typename T>
class small_height_tree_iterator;

struct Index {
	constexpr Index() = default;
	constexpr Index(std::size_t b, std::size_t v) : bfs(b), veb(v) {}

	bool operator==(const Index &other) const {
		return veb == other.veb and bfs == other.bfs;
	}

	bool operator!=(const Index &other) const { return not(*this == other); }

	std::size_t bfs = 0;
	std::size_t veb = 0;
};

using Path = std::vector<Index>;

struct FindResult {
	std::vector<Index> path;
	bool found;
};

template <typename T>
class small_height_tree {
private:
	static constexpr double DEFAULT_HEIGHT = 3;
	static constexpr Index ROOT_INDEX = {1, 1};
	static constexpr double FIRST_THRESHOLD = 0.5;

public:
	using iterator = small_height_tree_iterator<T>;

	explicit small_height_tree()
	    : max_height(DEFAULT_HEIGHT)
	    , nodes_used(0)
	    , tree_meta_manager()
	    , tree_meta(tree_meta_manager.get_tree_meta(height()))
	    , data(capacity() + 1) {}

	explicit small_height_tree(const std::size_t &height)
	    : max_height(height)
	    , nodes_used(0)
	    , tree_meta_manager()
	    , tree_meta(tree_meta_manager.get_tree_meta(height))
	    , data(capacity() + 1) {}

	iterator begin() noexcept { return begin(ROOT_INDEX); }

	[[nodiscard]] iterator begin(const Index &root) {
		if (not data_at(root).has_value()) {
			return end();
		}

		return iterator{this, leftmost(root), root};
	}

	iterator end() const noexcept {
		return iterator{};
	}

	iterator find(const T &value) {
		const auto &[path, found] = find_position(value);
		return found ? iterator{this, path.back()} : end();
	}

	std::pair<iterator, bool> insert(const T &value) {
		METRICS_RECORD_BLOCK("small_height_tree::insert");

		// Maintain the invariant: H > ceil(log2(n / FIRST_THRESHOLD + 1))
		const std::size_t prefered_height =
		    std::ceil(std::log2((nodes_used + 1) / FIRST_THRESHOLD + 1));
		if (max_height < prefered_height) {
			resize(prefered_height);
		}

		const auto &[path, found] = find_position(value);
		if (found) {
			return {iterator{this, path.back()}, false};
		}

		nodes_used++;

		// If the path leads to a leaf with a value, then the tree needs
		// rebalancing.
		if (data_at(path.back()).has_value() and path.size() - 1 == max_height) {
			rebalance(value, path);
			return {find(value), true};
		}

		data[path.back().veb] = value;

		return {iterator{this, path.back()}, true};
	}

	iterator erase(iterator pos) {
		iterator pos_copy{pos};

		if (not is_leaf(pos.index)) {
			auto next = [](auto iter, bool inc) { return inc ? ++iter : --iter; };
			bool has_right_child = data_at(right(pos.index)).has_value();

			while (not is_leaf(pos.index)) {
				auto next_pos = next(pos, has_right_child);
				std::iter_swap(pos, next_pos);
				pos = next_pos;
			}

			/*
			 * If a right child was initially present an increment is not needed
			 * because the initial pos was replaced with its successor.
			 */
			if (not has_right_child) {
				++pos_copy;
			}
		} else {
			++pos_copy;
		}

		// TODO: Rebalance the tree?

		pos->reset();
		nodes_used--;

		return pos_copy;
	}

	void rebalance(const T &extra_value, const Path &path) {
		METRICS_RECORD_BLOCK("small_height_tree::rebalance");

		const auto rebalance_tree = find_rebalacing_node(path);
		DEBUG_ASSERT(rebalance_tree.first != path.crend(), assert_config{},
		             "No node was found to rebalance on");

		std::vector<T> ordered_values;
		ordered_values.reserve(rebalance_tree.second + 1);

		inorder(*rebalance_tree.first, [&ordered_values](std::optional<T> &node) {
			ordered_values.push_back(node.value());
			node.reset();
		});

		// Insert the value that caused the rebalancing in the sorted vector.
		ordered_values.insert(
		    std::upper_bound(ordered_values.begin(), ordered_values.end(), extra_value),
		    extra_value);

		distribute(*rebalance_tree.first, ordered_values.begin(), ordered_values.end());
	}

	void resize(const std::size_t &new_height) {
		METRICS_RECORD_BLOCK("small_height_tree::resize");

		// Extract all the values from the tree and reset the nodes.
		std::vector<T> ordered_values;
		ordered_values.reserve(nodes_used);
		inorder(ROOT_INDEX, [&ordered_values](std::optional<T> &node) {
			ordered_values.push_back(node.value());
			node.reset();
		});

		// Resize the underlying vector
		tree_meta = tree_meta_manager.get_tree_meta(new_height);
		max_height = new_height;
		data.resize(size_of(new_height) + 1);

		// Put the values back.
		distribute(ROOT_INDEX, ordered_values.begin(), ordered_values.end());
	}

	void inorder(const Index &root, std::function<void(std::optional<T> &)> callback) {
		auto iter = begin(root);
		while (iter != end()) {
			callback(*iter);
			++iter;
		}
	}

	template <typename Iter>
	void distribute(const Index &root, Iter values_begin, Iter values_end) {
		Path path_to_root = compute_path(root);
		path_to_root.pop_back(); // Remove the root from the path. It will be added by
		                         // distribute_impl().

		// Decrement the end to ensure both begin and end point to actual values.
		distribute_impl(root, values_begin, values_end - 1, path_to_root);
	}

	[[nodiscard]] Index leftmost(const Index &root) {
		auto current = root;
		auto depth = static_cast<std::size_t>(std::log2(root.bfs));
		while (depth < max_height and data_at(left(current)).has_value()) {
			current = left(current);
			depth++;
		}

		return current;
	}

	    [[nodiscard]] std::size_t size_of_subtree(const Index &root) {
		std::size_t nodes_visited = 0;
		const auto count_nodes = [&nodes_visited](std::optional<T> & /*unused*/) {
			nodes_visited++;
		};
		inorder(root, count_nodes);

		return nodes_visited;
	}

	[[nodiscard]] inline double delta() { return (1 - FIRST_THRESHOLD) / max_height; }

	[[nodiscard]] inline double density_threshold(const std::size_t &depth) {
		return FIRST_THRESHOLD + depth * delta();
	}

	[[nodiscard]] std::size_t veb_index(const std::size_t &bfs_index,
	                                    const Path &path) const {
		const auto depth = std::log2(bfs_index);
		const auto &meta = tree_meta[depth];
		const auto current_subtree = bfs_index & meta.top_tree_size;

		return path[meta.top_tree_root_depth].veb + meta.top_tree_size +
		       current_subtree * meta.bottom_tree_size;
	}

	    [[nodiscard]] std::size_t veb_index(const std::size_t &bfs_index) const {
		const std::size_t max_depth = std::log2(bfs_index);
		std::vector<std::size_t> path;
		path.reserve(max_depth + 1);
		path.push_back(1);

		for (std::size_t depth = 1; depth <= max_depth; ++depth) {
			const auto bfs_at_depth = bfs_index >> (max_depth - depth);
			const auto veb_at_depth = path[tree_meta[depth].top_tree_root_depth] +
			                          tree_meta[depth].top_tree_size +
			                          (bfs_at_depth & tree_meta[depth].top_tree_size) *
			                              tree_meta[depth].bottom_tree_size;

			path.push_back(veb_at_depth);
		}

		return path.back();
	}

	[[nodiscard]] Index peer(const Index &index, const Path &path) {
		/*
		 * Compute the peer of a node in the tree.
		 * The result of computing the peer for the root is undefined.
		 */

		std::size_t peer_bfs;
		if (index.bfs & 1) { // Check if this node is the right child
			peer_bfs = index.bfs & ~1;
		} else {
			peer_bfs = index.bfs | 1;
		};

		return {peer_bfs, veb_index(peer_bfs, path)};
	}

	    [[nodiscard]] Index left(const Index &index, const Path &path) const {
		const auto left_bfs = index.bfs << 1U;
		return {left_bfs, veb_index(left_bfs, path)};
	}

	[[nodiscard]] Index left(const Index &index) const {
		const auto left_bfs = index.bfs << 1U;
		return {left_bfs, veb_index(left_bfs)};
	}

	    [[nodiscard]] Index right(const Index &index, const Path &path) const {
		const auto right_bfs = (index.bfs << 1U) + 1;
		return {right_bfs, veb_index(right_bfs, path)};
	}

	[[nodiscard]] Index right(const Index &index) const {
		const auto right_bfs = (index.bfs << 1U) + 1;
		return {right_bfs, veb_index(right_bfs)};
	}

	    [[nodiscard]] inline const std::size_t &size() {
		return nodes_used;
	}

	[[nodiscard]] inline const std::size_t &height() { return max_height; }

	[[nodiscard]] inline const std::optional<T> &data_at(const Index &index) const {
		return index.veb <= capacity() ? data[index.veb] : null_node;
	}

	[[nodiscard]] inline std::optional<T> &data_at(const Index &index) {
		return index.veb <= capacity() ? data[index.veb] : null_node;
	}

	[[nodiscard]] inline std::size_t size_of(const std::size_t &height) const {
		return (1U << (height + 1)) - 1; // The tree is rooted at depth 0, so we
		                                 // increment for the actual height.
	}

	[[nodiscard]] inline std::size_t capacity() const { return size_of(max_height); }

	[[nodiscard]] inline const implementation::TreeMeta &meta() const {
		return tree_meta;
	}

private:
	template <typename Iter>
	void distribute_impl(const Index &current,
	                     Iter values_begin,
	                     Iter values_end,
	                     Path &path) {
		DEBUG_ASSERT(current.veb < data.size(), assert_config{},
		             "Out of bounds veb index");

		if (values_begin > values_end) {
			return;
		}

		const auto middle = values_begin + std::distance(values_begin, values_end) / 2;
		data[current.veb] = *middle;

		const auto depth = path.size();
		if (depth < max_height) {
			path.push_back(current);

			distribute_impl(left(current, path), values_begin, middle - 1, path);
			distribute_impl(right(current, path), middle + 1, values_end, path);

			path.pop_back();
		}
	}

	[[nodiscard]] Path compute_path(const Index &to) {
		Path path;
		path.reserve(height() + 1);
		path.emplace_back(1, 1);

		const std::size_t max_depth = std::log2(to.bfs);
		for (std::size_t depth = 1; depth <= max_depth; ++depth) {
			const auto &current_meta = meta()[depth];

			const auto bfs_at_depth = to.bfs >> (max_depth - depth);
			const auto veb_at_depth = path[current_meta.top_tree_root_depth].veb +
			                          current_meta.top_tree_size +
			                          (bfs_at_depth & current_meta.top_tree_size) *
			                              current_meta.bottom_tree_size;

			path.emplace_back(bfs_at_depth, veb_at_depth);
		}

		return path;
	}

	    [[nodiscard]] bool is_leaf(const Index &index) {
		const std::size_t node_depth = std::log2(index.bfs);

		return node_depth == max_height or (not data_at(left(index)).has_value() and
		                                    not data_at(right(index)).has_value());
	}

	[[nodiscard]] FindResult find_position(const T &value) const {
		std::vector<Index> path;
		path.reserve(max_height + 1);

		Index current = ROOT_INDEX;
		for (std::size_t depth = 0; depth <= max_height; ++depth) {
			path.push_back(current);

			const auto &option = data_at(path[depth]);
			if (not option.has_value()) {
				return {path, false};
			}
			if (option.value() == value) {
				return {path, true};
			}

			if (depth < max_height) {
				if (value < option.value()) {
					current = left(current, path);
				}
				if (value > option.value()) {
					current = right(current, path);
				}
			}
		}

		return {path, false};
	}

	std::pair<Path::const_reverse_iterator, std::size_t> find_rebalacing_node(
	    const Path &path) {
		// Initially just the new element and the one in the leaf are counted
		std::size_t occupation = 2;

		// Substract twice as we start from the second to last depth
		std::size_t depth = path.size() - 1 - 1;

		for (auto iter = path.crbegin() + 1, prev = path.crbegin(); iter != path.crend();
		     prev = iter, ++iter) {
			// Add the peers of the child on the path and the current node (the
			// parent) to the count
			occupation += size_of_subtree(peer(*prev, path)) + 1;
			const auto density =
			    static_cast<double>(occupation) / size_of(max_height - depth);

			if (density <= density_threshold(depth--)) {
				return {iter, occupation};
			}
		}

		return {path.crend(), occupation};
	}

private:
	std::size_t max_height;
	std::size_t nodes_used;

	implementation::TreeMetaManager tree_meta_manager;
	implementation::TreeMeta tree_meta;

	std::optional<T> null_node = std::nullopt;
	std::vector<std::optional<T>> data;
};

template <typename T>
class small_height_tree_iterator {
public:
	using iterator_category = std::bidirectional_iterator_tag;
	using value_type = std::optional<T>;
	using reference = value_type &;
	using pointer = value_type *;
	using difference_type = std::ptrdiff_t;

	small_height_tree_iterator() : end(true){};

	small_height_tree_iterator(small_height_tree<T> *from,
	                           const Index current,
	                           const Index &subtree_root = {1, 1})
	    : end(false)
	    , index(current)
	    , root(subtree_root)
	    , tree(from)
	    , index_at_depth(compute_path(index)) {}

	small_height_tree_iterator(const small_height_tree_iterator &other) = default;

	~small_height_tree_iterator() = default;

	small_height_tree_iterator &operator=(const small_height_tree_iterator &other) =
	    default;

	bool operator!=(const small_height_tree_iterator &other) const {
		if (end or other.end) {
			return end != other.end;
		}

		return index != other.index;
	}

	small_height_tree_iterator &operator++() {
		/*
		 * Iteration scheme:
		 *
		 * The iteration is inorder, starting from the leftmost leaf node.
		 *
		 * Case 1: If the iterator points to a node that does not have a valid
		 * right child, it follows the chain of parents until it finds one which
		 * can be reached from the child to the left.
		 *
		 * Case 2: if the iterator points to a node that has a valid right
		 * child, it visits the child and continues on to the chain of left
		 * children that leads to a leaf node.
		 */

		auto depth = static_cast<std::size_t>(std::log2(index.bfs));
		if (not(depth < tree->height() and tree->data_at(right(index)).has_value())) {
			while (not is_root(index)) {
				const auto current_bfs_index = index.bfs;
				index = index_at_depth[--depth];
				if (index.bfs * 2 == current_bfs_index) {
					return *this;
				}
			}
		} else {
			index = right(index);
			index_at_depth[++depth] = index;

			while (depth < tree->height() and tree->data_at(left(index)).has_value()) {
				index = left(index);
				index_at_depth[++depth] = index;
			}

			return *this;
		}

		end = true;
		return *this;
	}

	/*
	 * Iteration scheme:
	 *
	 * The iteration is reverse inorder.
	 *
	 * Case 1: If the iterator points to a node that has a valid left child, it
	 * moves to the left child and then follows the chain of right children.
	 *
	 * Case 2: if the iterator points to a node that does not have a valid right
	 * child, it will move up the tree until a parent node is reached from the
	 * right child.
	 */
	small_height_tree_iterator &operator--() {
		auto depth = static_cast<std::size_t>(std::log2(index.bfs));

		if (tree->data_at(left(index)).has_value()) {
			index = left(index);
			index_at_depth[++depth] = index;

			while (tree->data_at(right(index)).has_value()) {
				index = right(index);
				index_at_depth[++depth] = index;
			}

			return *this;
		} else {
			for (size_t i = depth - 1; i >= 0; --i) {
				if (index_at_depth[i].bfs * 2 + 1 == index_at_depth[i + 1].bfs) {
					index = index_at_depth[i];
					return *this;
				}
			}

			end = true;
			return *this;
		}
	}

	reference operator*() const { return tree->data_at(index); };

	pointer operator->() const { return &(tree->data_at(index)); };

private:
	[[nodiscard]] Path compute_path(const Index &to) {
		Path path{tree->height() + 1};
		path[0] = {1, 1};

		const std::size_t max_depth = std::log2(to.bfs);
		for (std::size_t depth = 1; depth <= max_depth; ++depth) {
			const auto &current_meta = tree->meta()[depth];

			const auto bfs_at_depth = to.bfs >> (max_depth - depth);
			const auto veb_at_depth = path[current_meta.top_tree_root_depth].veb +
			                          current_meta.top_tree_size +
			                          (bfs_at_depth & current_meta.top_tree_size) *
			                              current_meta.bottom_tree_size;

			path[depth] = {bfs_at_depth, veb_at_depth};
		}

		return path;
	}

	    [[nodiscard]] bool is_root(const Index &node) {
		return node == root;
	}

	[[nodiscard]] std::size_t veb_index(const std::size_t &bfs_index) const {
		const auto depth = std::log2(bfs_index);
		const auto &meta = tree->meta()[depth];
		const auto current_subtree = bfs_index & meta.top_tree_size;

		return index_at_depth[meta.top_tree_root_depth].veb + meta.top_tree_size +
		       current_subtree * meta.bottom_tree_size;
	}

	    [[nodiscard]] Index left(const Index &current_index) const {
		const auto left_bfs_index = current_index.bfs << 1U;
		return {left_bfs_index, veb_index(left_bfs_index)};
	}

	[[nodiscard]] Index right(const Index &current_index) const {
		const auto right_bfs_index = (current_index.bfs << 1U) + 1;
		return {right_bfs_index, veb_index(right_bfs_index)};
	}

	private : template <class>
	          friend class small_height_tree;

private:
	bool end;

	Index index;
	Index root;
	small_height_tree<T> *tree;
	Path index_at_depth;
};

} // namespace cov
