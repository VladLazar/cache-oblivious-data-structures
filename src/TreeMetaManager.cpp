#include "TreeMetaManager.hpp"

namespace cov::implementation {

const TreeMeta &TreeMetaManager::get_tree_meta(const std::size_t &height) {
	auto iter = tree_meta_cache.find(height);
	if (iter == tree_meta_cache.end()) {
		iter = tree_meta_cache.emplace(height, TreeMeta(height)).first;
	}

	return iter->second;
}

} // namespace cov::implementation
