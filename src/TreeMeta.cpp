#include "TreeMeta.hpp"

#include <stack>
#include <vector>

namespace cov::implementation {

TreeMeta::TreeMeta(const std::size_t &height) : meta(compute_meta(height)) {}

bool TreeMeta::operator==(const TreeMeta &other) const {
	return tree_height() == other.tree_height();
}

bool TreeMeta::operator!=(const TreeMeta &other) const { return not(*this == other); }

std::size_t TreeMeta::tree_height() const { return meta.size(); }

std::vector<DepthMeta> TreeMeta::compute_meta(const std::size_t &max_depth) {
	std::vector<DepthMeta> resulting_meta(max_depth + 1);
	resulting_meta[0] = {0, 0, 0};

	std::stack<std::pair<std::size_t, std::size_t>> next_trees(
	    {std::make_pair(0, max_depth)});
	while (not next_trees.empty()) {
		const auto top_tree = next_trees.top();
		next_trees.pop();

		if (top_tree.first == top_tree.second) {
			continue;
		}

		const auto split_depth = top_tree.first + (top_tree.second - top_tree.first) / 2;
		const auto bottom_tree_size = size_of_tree(top_tree.second - split_depth - 1);
		const auto top_tree_size = size_of_tree(split_depth - top_tree.first);
		if (split_depth + 1 < resulting_meta.size()) {
			resulting_meta[split_depth + 1] = {bottom_tree_size, top_tree_size,
			                                   top_tree.first};
		}

		if (top_tree.second - top_tree.first > 1) {
			next_trees.push({split_depth + 1, top_tree.second});
			next_trees.push({top_tree.first, split_depth});
		}
	}

	return resulting_meta;
}

std::size_t TreeMeta::size_of_tree(const std::size_t &max_depth) const {
	return (1u << (max_depth + 1)) - 1;
}

bool DepthMeta::operator==(const cov::implementation::DepthMeta &other) const {
	return bottom_tree_size == other.bottom_tree_size and
	       top_tree_size == other.top_tree_size and
	       top_tree_root_depth == other.top_tree_root_depth;
}

bool DepthMeta::operator!=(const cov::implementation::DepthMeta &other) const {
	return not(*this == other);
}

std::ostream &operator<<(std::ostream &os, const cov::implementation::DepthMeta &meta) {
	os << "{ B: " << meta.bottom_tree_size << ", T: " << meta.top_tree_size
	   << ", D: " << meta.top_tree_root_depth << " }";
	return os;
}

} // namespace cov::implementation
