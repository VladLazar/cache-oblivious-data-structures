#!/bin/bash

main_dir="../$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
build_dir="$main_dir/build"

mkdir -p build && cd build

echo "Building the library and its dependencies ..."
cmake -DCMAKE_BUILD_TYPE=Release ../
cmake --build . -j4

echo "Starting the experiment. If you wish to stop the experiment the plots can still be created manually with the collected data."

chmod +x ./script/run_benchmarks.sh
./script/run_benchmarks.sh $1

echo "The experiment has finished. Generating plots ..."
tar -cvf ./results.tar ./BM_* > /dev/null

if [ "$1" = "static" ]; then
    python3 ./script/extract.py ./results.tar | python3 ./script/plot.py static
else
    python3 ./script/extract.py ./results.tar | python3 ./script/plot.py dynamic inorder
    python3 ./script/extract.py ./results.tar | python3 ./script/plot.py dynamic random
fi

echo "Plots have been generated in: cache-oblivious-data-structures/build/plots"
mkdir -p ./plots
mv *.png ./plots

