#!/bin/bash

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ "$1" = "static" ]; then
    benchmark_path="$script_dir/../benchmark/static_tree_benchmark"
    benchmarks=(BM_BinarySearch BM_StaticBfsBranchy BM_StaticBfsBranchFree BM_StaticVeb)
else
    benchmark_path="$script_dir/../benchmark/bench"
    
    benchmarks=(BM_StdSetInorderInsertion BM_StdSetRandomInsertion         
                BM_CovTreeInorderInsertion BM_CovTreeRandomInsertion       
                BM_HybridTreeInorderInsertion BM_HybridTreeRandomInsertion
                BM_SmallHeightTreeInorderInsertion BM_SmallHeightTreeRandomInsertion
                BM_AbslBTreeSetInorderInsertion BM_AbslBTreeSetRandomInsertion)
fi

elements_in_benchmark=$(python3 "$script_dir/seq.py")
for item in ${elements_in_benchmark[*]}
do
    export ELEMENTS="$item"

    for bench in ${benchmarks[*]}
    do
        echo "Running $bench with $item elements ..."

        local_dir=$(printf "%s_%s" "$bench" "$item")
        mkdir -p "$local_dir"

        results_file="$(pwd)/$local_dir/res"
        stats_file="$(pwd)/$local_dir/stats"

        benchmark_command="$benchmark_path --benchmark_out_format=json --benchmark_out=$results_file --benchmark_filter=$bench"
        perf_command="perf stat -e cache-references,cache-misses,branch-misses,page-faults -o $stats_file $benchmark_command"

        eval "$perf_command"
    done
done
