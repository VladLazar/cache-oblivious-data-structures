import sys
import json
import math

import matplotlib.pyplot as plt
plt.style.use('seaborn-deep')

ELEMENT_SIZE = 4 # bytes
SAVE_FIGURES = True

DISPLAY_NAME = {
    'BM_BinarySearch': 'binary search',
    'BM_StaticBfsBranchy': 'static branchy BFS layout',
    'BM_StaticBfsBranchFree': 'static branch-free BFS layout',
    'BM_StaticVeb': 'static vEB layout',

    'BM_StdSetInorderInsertion': 'std::set',
    'BM_CovTreeInorderInsertion': 'Bender tree',
    'BM_HybridTreeInorderInsertion': 'Hybrid tree',
    'BM_SmallHeightTreeInorderInsertion': 'Small tree',
    'BM_AbslBTreeSetInorderInsertion': 'Google Abseil BTree',

    'BM_StdSetRandomInsertion': 'std::set',
    'BM_CovTreeRandomInsertion': 'Bender tree',
    'BM_HybridTreeRandomInsertion': 'Hybrid tree',
    'BM_SmallHeightTreeRandomInsertion': 'Small tree',
    'BM_AbslBTreeSetRandomInsertion': 'Google Abseil BTree'
}

def select_benchmarks(args):
    if args[0] == 'static':
        return ['BM_BinarySearch', 'BM_StaticBfsBranchy', 'BM_StaticBfsBranchFree', 'BM_StaticVeb']
    elif args[0] == 'dynamic':
        if args[1] == 'inorder':
            return ['BM_StdSetInorderInsertion',
                    'BM_CovTreeInorderInsertion',
                    'BM_HybridTreeInorderInsertion',
                    'BM_SmallHeightTreeInorderInsertion',
                    'BM_AbslBTreeSetInorderInsertion']
        elif args[1] == 'random':
            return ['BM_StdSetRandomInsertion',
                    'BM_CovTreeRandomInsertion',
                    'BM_HybridTreeRandomInsertion',
                    'BM_SmallHeightTreeRandomInsertion',
                    'BM_AbslBTreeSetRandomInsertion']
    else:
        return args

def add_to_plot(benchmark_name, x_axis, y_axis, linestyle='-'):
    x = [math.log(res[x_axis], 2) for res in data[benchmark_name]]

    if y_axis is 'time':
        y = [res[y_axis] / (10 ** 9) for res in data[benchmark_name]]
    else:
        y = [res[y_axis] / res['iterations'] for res in data[benchmark_name]]

    plt.plot(x, y, label=DISPLAY_NAME.get(benchmark_name, benchmark_name), linestyle=linestyle, marker='.')

    ticks = [i for i in range(int(max(x)) + 1) if i % 2 == 0]
    labels = ['$2^{%d}$' % (i) for i in ticks]
    plt.xticks(ticks, labels)


def add_cache_size_indicators():
    cache_sizes = [ (math.log(16 * 1024 / ELEMENT_SIZE, 2), 'r')
                  , (math.log(512 * 1024 / ELEMENT_SIZE, 2), 'b')]

    for i, (cache_size, color) in enumerate(cache_sizes):
        plt.axvline(x=cache_size, label='L{} cache size'.format(i + 1), ls='--', color=color)

if __name__ == '__main__':
    data = json.load(sys.stdin)

    metrics = ['time', 'cache-misses', 'branch-misses', 'page-faults']
    metric_names = {'time': 'Wall time', 'cache-misses': 'Cache misses', 'page-faults': 'Page faults', 'branch-misses': 'Branch misses'}
    benchmarks = select_benchmarks(sys.argv[1:])

    for metric in metrics:
        plt.figure()

        # add_cache_size_indicators()
        for bench in benchmarks:
            add_to_plot(bench, 'elements-used', metric)

        plt.title('{} of $3\cdot 10^6$ searches'.format(metric_names[metric]))

        plt.xlabel('elements inserted')
        plt.ylabel('seconds' if metric == 'time' else '{}'.format(metric))
        plt.legend()

        if SAVE_FIGURES:
            experiment_name = sys.argv[1]
            if len(sys.argv) > 2:
                experiment_name += '-' + sys.argv[2]

            plt.savefig('{}-{}.png'.format(experiment_name, metric))

    plt.show()
