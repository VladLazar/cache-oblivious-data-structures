import sys
import json
import tarfile
import shutil
from pathlib import Path

PERF_COUNTERS = ['cache-references', 'cache-misses', 'page-faults', 'branch-misses']

def contains_counter(string):
    for counter in PERF_COUNTERS:
        if counter in string:
            return counter

    return None

def extract_elements_from_name(dirname):
    return int(dirname.split('_')[-1])

def extract_from_res_file(path):
    with open(path) as desc:
        data = json.load(desc)
        
        assert len(data['benchmarks']) == 1
        return (data['benchmarks'][0]['name'], data['benchmarks'][0]['real_time'], data['benchmarks'][0]['iterations'], data['benchmarks'][0]['sum_found'])

def extract_from_stats_file(path):
    with open(path) as fd:
        counters = {}

        for line in fd:
            counter = contains_counter(line)
            if counter is not None:
                words = line.split()

                counter_value = int(words[0].replace(',', ''))
                counters[counter] = counter_value

        return counters

def extract_from_dump_dir(dump_dir):
    res_path = dump_dir / 'res'
    stats_path = dump_dir / 'stats'

    name, time, iterations, total_sum = extract_from_res_file(res_path)
    counters = extract_from_stats_file(stats_path)

    counters['time'] = time
    counters['iterations'] = iterations
    counters['total_sum'] = total_sum

    return (name, counters)

def extract_from_dir(directory):
    results_per_benchmark = {}
    malformed = []

    result_directories = Path(directory).glob('**/BM_*')
    result_directories  = sorted(result_directories, key=lambda path: extract_elements_from_name(path.name))

    for directory in result_directories:
        if (directory.parts[-1].endswith('_0')):
            continue

        elements_used = extract_elements_from_name(directory.name)
        try:
            benchmark_name, extracted = extract_from_dump_dir(directory)
            extracted['elements-used'] = elements_used

            current = results_per_benchmark.get(benchmark_name, [])
            current.append(extracted)

            results_per_benchmark[benchmark_name] = current
        except:
            malformed.append(elements_used)
            print('Directory {} contains malformed data'.format(directory), file=sys.stderr)

    # Remove the malformed ones 
    #for bench_name, results in results_per_benchmark.items():
    #    results = [x for x in results if not x['elements-used'] in malformed]
    #    results_per_benchmark[bench_name] = results

    return results_per_benchmark

if __name__ == '__main__':
    argc = len(sys.argv)

    if argc != 2:
        sys.exit("Usage: python3 extract.py <path_to_data_tar>")

    tarf = tarfile.open(sys.argv[-1])
    tarf.extractall('./tmp')

    json.dump(extract_from_dir('./tmp'), sys.stdout)

    shutil.rmtree('./tmp')
