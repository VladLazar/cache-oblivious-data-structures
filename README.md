# Cache Oblivious Data Structures 

## What?
This repository contains a library of [cache oblivious](https://en.wikipedia.org/wiki/Cache-oblivious_algorithm) search tree data structures and
benchmarks that that compare them to their clasic or cache aware (e.g. B-tree) counterparts.

### Contents of the library
The library contains both static and dynamic search trees. The static ones do not permit insertions and deletions after their creation.
Here is a list with the library contents and references where relevant:

* Static Trees
    + Eytzinger (or BFS) layout tree
    + van Emde Boas layout tree [1](https://www.brics.dk/RS/01/36/BRICS-RS-01-36.pdf)
* Dynamic Trees
    + Cache Oblivious B-tree by Bender et al. [2](https://erikdemaine.org/papers/FOCS2000b/paper.pdf)
    + Hybrid tree, a variation on the proposal of Bender that uses the Eytzinger layout
    + Cache oblivious trees of small height of Brodal et al. [1](https://www.brics.dk/RS/01/36/BRICS-RS-01-36.pdf)

### Experiments
The code for two experiments is bundled with the code: one involving static trees and another one on dynamic trees.
Both are very similar: generate a number of values and insert them into the tree and then perform a fixed number of searches.
Various metrics are collected during each run such as: wall time, cache misses, page faults and branch misses. These experiments
build on the work of Morin and Khuong [3](https://arxiv.org/abs/1509.05053).

## Why?
To find out if cache oblivious data structures can be faster than traditional data structures.

## How?
Reproducing the experiments should be very easy. This section describes how to do it.

### Requirements
* A Linux system
* Python3 with the following with the matplotlib package installed: `pip install matplotlib`
* [perf](https://perf.wiki.kernel.org/index.php/Main_Page): On Ubuntu: `sudo apt-get install linux-tools`
* [CMake](https://cmake.org/) with a newer version than 3.10

### Instructions
To run the experiments on your machine:
1. `git clone --recursive <git repo url>`
2. `cd cache-oblivious-data-structures`
3. `chmod +x script/build_and_run_experiments.sh` to make the script executable
4. `./script/build_and_run_experiments.sh` for the dynamic trees experiment or `./script/build_and_run_experiments.sh static` for the
static trees experiment (Note that you may need to run this with sudo privileges to allow perf to collect the data)
5. (optional) Run the tests for the library and its dependencies: `cd build && make test` 

The script will build and the code and run the benchmarks. The maximum size of the dataset used is a third of the
RAM memory available on the machine, or 0.5GB if that cannot be determined. When the script finishes it will display
all the plots it has generated and save them in `./build/plots`.
