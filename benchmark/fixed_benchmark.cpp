#include <benchmark/benchmark.h>

#include <cassert>
#include <chrono>
#include <cstdlib>
#include <random>
#include <set>

#include "benchmark_config.hpp"
#include "cov_tree.hpp"
#include "hybrid_tree.hpp"
#include "small_height_tree.hpp"

#include <mtr/metrics.hpp>

static void BM_StdSetInorderInsertion() {
	METRICS_RECORD_BLOCK("BM_StdSetInorderInsertion");

	std::set<std::uint32_t> values;

	for (std::uint32_t val = 0; val < ELEMENTS; ++val) {
		benchmark::DoNotOptimize(values.insert(val));
	}
}

static void BM_SmallHeightTreeInorderInsertion() {
	METRICS_RECORD_BLOCK("BM_SmallHeightTreeInorderInsertion");

	cov::small_height_tree<std::uint32_t> values{3};

	for (std::uint32_t val = 0; val < ELEMENTS; ++val) {
		benchmark::DoNotOptimize(values.insert(val));
	}
}

static void BM_CovTreeInorderInsertion() {
	METRICS_RECORD_BLOCK("BM_CovTreeInorderInsertion");

	cov::cov_tree<std::uint32_t> values;

	for (std::uint32_t val = 0; val < ELEMENTS; ++val) {
		benchmark::DoNotOptimize(values.insert(val));
	}
}

static void BM_HybridTreeInorderInsertion() {
	METRICS_RECORD_BLOCK("BM_HybridTreeInorderInsertion");

	cov::hybrid_tree<std::uint32_t> values;

	for (std::uint32_t val = 0; val < ELEMENTS; ++val) {
		benchmark::DoNotOptimize(values.insert(val));
	}
}
static void BM_StdSetRandomInsertion() {
	METRICS_RECORD_BLOCK("BM_StdSetRandomInsertion");

	std::mt19937 rng{SEED};
	std::uniform_int_distribution<std::uint32_t> dist{};

	std::set<std::uint32_t> values;

	for (std::uint32_t val = 0; val < ELEMENTS; ++val) {
		benchmark::DoNotOptimize(values.insert(dist(rng)));
	}
}

static void BM_SmallHeightTreeRandomInsertion() {
	METRICS_RECORD_BLOCK("BM_SmallHeightTreeRandomInsertion");

	std::mt19937 rng{SEED};
	std::uniform_int_distribution<std::uint32_t> dist{};

	cov::small_height_tree<std::uint32_t> values{3};

	for (std::uint32_t val = 0; val < ELEMENTS; ++val) {
		benchmark::DoNotOptimize(values.insert(dist(rng)));
	}
}

static void BM_CovTreeRandomInsertion() {
	METRICS_RECORD_BLOCK("BM_CovTreeRandomInsertion");

	std::mt19937 rng{SEED};
	std::uniform_int_distribution<std::uint32_t> dist{};

	cov::cov_tree<std::uint32_t> values;

	for (std::uint32_t val = 0; val < ELEMENTS; ++val) {
		benchmark::DoNotOptimize(values.insert(dist(rng)));
	}
}

static void BM_HybridTreeRandomInsertion() {
	METRICS_RECORD_BLOCK("BM_HybridTreeRandomInsertion");

	std::mt19937 rng{SEED};
	std::uniform_int_distribution<std::uint32_t> dist{};

	cov::hybrid_tree<std::uint32_t> values;

	for (std::uint32_t val = 0; val < ELEMENTS; ++val) {
		benchmark::DoNotOptimize(values.insert(dist(rng)));
	}
}

static void BM_StdSetUseCase() {
	METRICS_RECORD_BLOCK("BM_StdSetUseCase");

	std::set<std::uint32_t> values;

	for (std::uint32_t val = 0; val < ELEMENTS; ++val) {
		values.insert(val);
	}

	std::uint32_t total = 0;
	for (const auto &x : values) {
		total += x;
	}

	for (std::uint32_t val = 0; val < ELEMENTS; ++val) {
		const auto iter = values.find(val);
		values.erase(iter);
	}
}

static void BM_SmallHeightTreeUseCase() {
	METRICS_RECORD_BLOCK("BM_SmallHeightTreeUseCase");

	cov::small_height_tree<std::uint32_t> values;

	for (std::uint32_t val = 0; val < ELEMENTS; ++val) {
		values.insert(val);
	}

	std::uint32_t total = 0;
	for (const auto &x : values) {
		total += *x;
	}

	for (std::uint32_t val = 0; val < ELEMENTS; ++val) {
		const auto iter = values.find(val);
		values.erase(iter);
	}
}

static void BM_CovTreeUseCase() {
	METRICS_RECORD_BLOCK("BM_CovTreeUseCase");

	cov::cov_tree<std::uint32_t> values;

	for (std::uint32_t val = 0; val < ELEMENTS; ++val) {
		values.insert(val);
	}

	std::uint32_t total = 0;
	for (const auto &x : values) {
		total += x;
	}

	for (std::uint32_t val = 0; val < ELEMENTS; ++val) {
		const auto iter = values.find(val);
		values.erase(iter);
	}
}

static void BM_HybridTreeUseCase() {
	METRICS_RECORD_BLOCK("BM_HybridTreeUseCase");

	cov::hybrid_tree<std::uint32_t> values;

	for (std::uint32_t val = 0; val < ELEMENTS; ++val) {
		values.insert(val);
	}

	std::uint32_t total = 0;
	for (const auto &x : values) {
		total += x;
	}

	for (std::uint32_t val = 0; val < ELEMENTS; ++val) {
		const auto iter = values.find(val);
		values.erase(iter);
	}
}

template <typename Func>
static void run_benchmark(Func &&func, std::size_t iterations) {
	for (std::size_t i = 0; i < iterations; ++i) {
		func();
	}
}

int main(int argc, char *argv[]) {
	if (argc != 2) {
		std::cerr << "Wrong number of args!" << std::endl;
	}

	char *end;
	const std::size_t iters = std::strtoul(argv[1], &end, 10);

	run_benchmark(BM_StdSetInorderInsertion, iters);
	run_benchmark(BM_SmallHeightTreeInorderInsertion, iters);
	run_benchmark(BM_CovTreeInorderInsertion, iters);
	run_benchmark(BM_HybridTreeInorderInsertion, iters);

	run_benchmark(BM_StdSetRandomInsertion, iters);
	run_benchmark(BM_SmallHeightTreeRandomInsertion, iters);
	run_benchmark(BM_CovTreeRandomInsertion, iters);
	run_benchmark(BM_HybridTreeRandomInsertion, iters);

	run_benchmark(BM_StdSetUseCase, iters);
	run_benchmark(BM_SmallHeightTreeUseCase, iters);
	run_benchmark(BM_CovTreeUseCase, iters);
	run_benchmark(BM_HybridTreeUseCase, iters);

	const mtr::metric_aggregator &agr = mtr::metric_aggregator::instance();

	using ns = std::chrono::nanoseconds;

	std::cout << "Running benchmarks with " << ELEMENTS << " elements" << std::endl;

	agr.dump_metrics<ns>("BM_StdSetInorderInsertion", std::cout);
	agr.dump_metrics<ns>("BM_SmallHeightTreeInorderInsertion", std::cout);
	agr.dump_metrics<ns>("BM_CovTreeInorderInsertion", std::cout);
	agr.dump_metrics<ns>("BM_HybridTreeInorderInsertion", std::cout);

	agr.dump_metrics<ns>("BM_StdSetRandomInsertion", std::cout);
	agr.dump_metrics<ns>("BM_SmallHeightTreeRandomInsertion", std::cout);
	agr.dump_metrics<ns>("BM_CovTreeRandomInsertion", std::cout);
	agr.dump_metrics<ns>("BM_HybridTreeRandomInsertion", std::cout);

	agr.dump_metrics<ns>("BM_StdSetUseCase", std::cout);
	agr.dump_metrics<ns>("BM_SmallHeightTreeUseCase", std::cout);
	agr.dump_metrics<ns>("BM_CovTreeUseCase", std::cout);
	agr.dump_metrics<ns>("BM_HybridTreeUseCase", std::cout);

	return 0;
}
