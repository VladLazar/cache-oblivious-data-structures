#include <benchmark/benchmark.h>

#include <cassert>
#include <cstdint>
#include <random>
#include <set>

#include "benchmark_config.hpp"
#include "cov_tree.hpp"
#include "hybrid_tree.hpp"
#include "small_height_tree.hpp"

#include "absl/container/btree_set.h"

using inserted_type = std::uint32_t;
static constexpr uint64_t SEARCHES = 3 * 1'000'000;

static void BM_StdSetInorderInsertion(benchmark::State& state) {
	inorder_generator<inserted_type> gen;

	uint64_t sum = 0;
	uint64_t iterations = 0;
	for (auto _ : state) {
		std::set<inserted_type> values;

		for (std::size_t val = 0; val < ELEMENTS; ++val) {
			benchmark::DoNotOptimize(values.insert(gen.next()));
		}

		for (std::size_t i = 0; i < SEARCHES; ++i) {
			const auto iter = values.find(gen.next());
			if (iter != values.end()) {
				sum += *iter;
			}
		}

		gen.reset();
		++iterations;
	}

	state.counters["sum_found"] = static_cast<int>(sum / iterations);
}

static void BM_SmallHeightTreeInorderInsertion(benchmark::State& state) {
	inorder_generator<inserted_type> gen;

	uint64_t sum = 0;
	uint64_t iterations = 0;
	for (auto _ : state) {
		cov::small_height_tree<inserted_type> values;

		for (std::size_t val = 0; val < ELEMENTS; ++val) {
			benchmark::DoNotOptimize(values.insert(gen.next()));
		}

		for (std::size_t i = 0; i < SEARCHES; ++i) {
			const auto iter = values.find(gen.next());
			if (iter != values.end()) {
				sum += iter->value();
			}
		}

		gen.reset();
		++iterations;
	}

	state.counters["sum_found"] = static_cast<int>(sum / iterations);
}

static void BM_CovTreeInorderInsertion(benchmark::State& state) {
	inorder_generator<inserted_type> gen;

	uint64_t sum = 0;
	uint64_t iterations = 0;
	for (auto _ : state) {
		cov::cov_tree<inserted_type> values;

		for (std::size_t val = 0; val < ELEMENTS; ++val) {
			benchmark::DoNotOptimize(values.insert(gen.next()));
		}

		for (std::size_t i = 0; i < SEARCHES; ++i) {
			const auto iter = values.find(gen.next());
			if (iter != values.end()) {
				sum += *iter;
			}
		}

		gen.reset();
		++iterations;
	}

	state.counters["sum_found"] = static_cast<int>(sum / iterations);
}

static void BM_HybridTreeInorderInsertion(benchmark::State& state) {
	inorder_generator<inserted_type> gen;

	uint64_t sum = 0;
	uint64_t iterations = 0;
	for (auto _ : state) {
		cov::hybrid_tree<inserted_type> values;

		for (std::size_t val = 0; val < ELEMENTS; ++val) {
			benchmark::DoNotOptimize(values.insert(gen.next()));
		}

		for (std::size_t i = 0; i < SEARCHES; ++i) {
			const auto iter = values.find(gen.next());
			if (iter != values.end()) {
				sum += *iter;
			}
		}

		gen.reset();
		++iterations;
	}

	state.counters["sum_found"] = static_cast<int>(sum / iterations);
}

static void BM_AbslBTreeSetInorderInsertion(benchmark::State& state) {
	inorder_generator<inserted_type> gen;

	uint64_t sum = 0;
	uint64_t iterations = 0;
	for (auto _ : state) {
		absl::btree_set<inserted_type> values;

		for (std::size_t val = 0; val < ELEMENTS; ++val) {
			benchmark::DoNotOptimize(values.insert(gen.next()));
		}

		for (std::size_t i = 0; i < SEARCHES; ++i) {
			const auto iter = values.find(gen.next());
			if (iter != values.end()) {
				sum += *iter;
			}
		}

		gen.reset();
		++iterations;
	}

	state.counters["sum_found"] = static_cast<int>(sum / iterations);
}

static void BM_StdSetRandomInsertion(benchmark::State& state) {
	random_generator<inserted_type> gen(0, 2 * ELEMENTS + 2);

	uint64_t sum = 0;
	uint64_t iterations = 0;
	for (auto _ : state) {

		std::set<inserted_type> values;

		for (std::size_t val = 0; val < ELEMENTS; ++val) {
			benchmark::DoNotOptimize(values.insert(gen.next()));
		}

		for (std::size_t i = 0; i < SEARCHES; ++i) {
			const auto iter = values.find(gen.next());
			if (iter != values.end()) {
				sum += *iter;
			}
		}

		gen.reset();
		++iterations;
	}

	state.counters["sum_found"] = static_cast<int>(sum / iterations);
}

static void BM_SmallHeightTreeRandomInsertion(benchmark::State& state) {
	random_generator<inserted_type> gen(0, 2 * ELEMENTS + 2);

	uint64_t sum = 0;
	uint64_t iterations = 0;
	for (auto _ : state) {
		cov::small_height_tree<inserted_type> values;

		for (std::size_t val = 0; val < ELEMENTS; ++val) {
			benchmark::DoNotOptimize(values.insert(gen.next()));
		}

		for (std::size_t i = 0; i < SEARCHES; ++i) {
			const auto iter = values.find(gen.next());
			if (iter != values.end()) {
				sum += iter->value();
			}
		}

		gen.reset();
		++iterations;
	}

	state.counters["sum_found"] = static_cast<int>(sum / iterations);
}

static void BM_CovTreeRandomInsertion(benchmark::State& state) {
	random_generator<inserted_type> gen(0, 2 * ELEMENTS + 2);

	uint64_t sum = 0;
	uint64_t iterations = 0;
	for (auto _ : state) {
		cov::cov_tree<inserted_type> values;

		for (std::size_t val = 0; val < ELEMENTS; ++val) {
			benchmark::DoNotOptimize(values.insert(gen.next()));
		}

		for (std::size_t i = 0; i < SEARCHES; ++i) {
			const auto iter = values.find(gen.next());
			if (iter != values.end()) {
				sum += *iter;
			}
		}

		gen.reset();
		++iterations;
	}

	state.counters["sum_found"] = static_cast<int>(sum / iterations);
}

static void BM_HybridTreeRandomInsertion(benchmark::State& state) {
	random_generator<inserted_type> gen(0, 2 * ELEMENTS + 2);

	uint64_t sum = 0;
	uint64_t iterations = 0;
	for (auto _ : state) {
		cov::hybrid_tree<inserted_type> values;

		for (std::size_t val = 0; val < ELEMENTS; ++val) {
			benchmark::DoNotOptimize(values.insert(gen.next()));
		}

		for (std::size_t i = 0; i < SEARCHES; ++i) {
			const auto iter = values.find(gen.next());
			if (iter != values.end()) {
				sum += *iter;
			}
		}

		gen.reset();
		++iterations;
	}

	state.counters["sum_found"] = static_cast<int>(sum / iterations);
}

static void BM_AbslBTreeSetRandomInsertion(benchmark::State& state) {
	random_generator<inserted_type> gen(0, 2 * ELEMENTS + 2);

	uint64_t sum = 0;
	uint64_t iterations = 0;
	for (auto _ : state) {
		absl::btree_set<inserted_type> values;

		for (std::size_t val = 0; val < ELEMENTS; ++val) {
			benchmark::DoNotOptimize(values.insert(gen.next()));
		}

		for (std::size_t i = 0; i < SEARCHES; ++i) {
			const auto iter = values.find(gen.next());
			if (iter != values.end()) {
				sum += *iter;
			}
		}

		gen.reset();
		++iterations;
	}

	state.counters["sum_found"] = static_cast<int>(sum / iterations);
}

// Register the function as a benchmark
BENCHMARK(BM_StdSetInorderInsertion);
BENCHMARK(BM_SmallHeightTreeInorderInsertion);
BENCHMARK(BM_CovTreeInorderInsertion);
BENCHMARK(BM_HybridTreeInorderInsertion);
BENCHMARK(BM_AbslBTreeSetInorderInsertion);

BENCHMARK(BM_StdSetRandomInsertion);
BENCHMARK(BM_SmallHeightTreeRandomInsertion);
BENCHMARK(BM_CovTreeRandomInsertion);
BENCHMARK(BM_HybridTreeRandomInsertion);
BENCHMARK(BM_AbslBTreeSetRandomInsertion);

int main(int argc, char** argv) {
	std::cout << "Running benchmarks with " << ELEMENTS << " elements" << std::endl;

	::benchmark::Initialize(&argc, argv);
	if (::benchmark::ReportUnrecognizedArguments(argc, argv)) {
		return 1;
	}

	::benchmark::RunSpecifiedBenchmarks();
}
