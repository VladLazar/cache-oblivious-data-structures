#pragma once

#include <cstdlib>
#include <random>

#define ITERATIONS 10
#define DEFAULT_ELEMENTS 1000

static std::size_t get_elements_from_env(std::size_t default_value) {
	const auto elements_env_var = std::getenv("ELEMENTS");

	char *end;
	return elements_env_var == nullptr ? default_value :
	                                     std::strtoul(elements_env_var, &end, 10);
}

static const std::size_t ELEMENTS = get_elements_from_env(DEFAULT_ELEMENTS);
//static const unsigned int SEED = 70375847;
static const unsigned int SEED = 232342;

template <typename T>
class generator {
public:
	virtual ~generator() = default;
	virtual T next() = 0;
	virtual void reset() = 0;
};

template <typename T>
class random_generator : public generator<T> {
public:
	explicit random_generator() : rng_(seed_) {}
	explicit random_generator(T min, T max) : rng_(seed_), dist_(min, max) {}

	T next() override { return dist_(rng_); }

	void reset() override { rng_ = std::mt19937(seed_); }

private:
	const unsigned int seed_ = SEED;

	std::mt19937 rng_;
	std::uniform_int_distribution<T> dist_;
};

template <typename T>
class inorder_generator : public generator<T> {
public:
	T next() override { return current_++; }

	void reset() override { current_ = 0; }

private:
	T current_ = 0;
};
