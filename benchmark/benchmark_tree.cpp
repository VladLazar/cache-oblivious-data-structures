#include <benchmark/benchmark.h>
#include <bits/stdint-uintn.h>

#include <chrono>
#include <iostream>
#include <set>

#include "benchmark_config.hpp"
#include "cov_tree.hpp"
#include "hybrid_tree.hpp"
#include "small_height_tree.hpp"

#include "absl/container/btree_set.h"
#include "mtr/metrics.hpp"

template <typename Container, typename Generator>
static inline void benchmark_function(Generator *const gen) {
	Container values;

	for (std::size_t val = 1; val <= ELEMENTS; ++val) {
		benchmark::DoNotOptimize(values.insert(gen->next()));
	}
}

template <typename Container, typename Generator>
static void run(Generator *const gen) {
	for (int i = 0; i < ITERATIONS; ++i) {
		gen->reset();

		METRICS_RECORD_BLOCK("benchmark");
		benchmark_function<Container, Generator>(gen);
	}
}

static void print_results(const mtr::metric_aggregator &aggregator) {
	using ns = std::chrono::nanoseconds;
	aggregator.dump_all<ns>(std::cout);
}

int main(int argc, char *argv[]) {
	if (argc != 3) {
		std::cerr << "Wrong number of arguments" << std::endl;
	}

	const std::string tree_type(argv[1]);
	const std::string benchmark_type(argv[2]);

	generator<uint32_t> *gen;
	if (benchmark_type == "inorder") {
		gen = new inorder_generator<uint32_t>();
	} else {
		gen = new random_generator<uint32_t>();
	}

	if (tree_type == "std") {
		run<std::set<uint32_t>>(gen);
	}

	if (tree_type == "cov_tree") {
		run<cov::cov_tree<uint32_t>>(gen);
	}

	if (tree_type == "hybrid_tree") {
		run<cov::hybrid_tree<uint32_t>>(gen);
	}

	if (tree_type == "small_tree") {
		run<cov::small_height_tree<uint32_t>>(gen);
	}

	if (tree_type == "btree") {
		run<absl::btree_set<uint32_t>>(gen);
	}

	print_results(mtr::metric_aggregator::instance());
}
