#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <optional>
#include <variant>

#include "veb_index.hpp"

using namespace ::testing;
using namespace cov::implementation;

TEST(veb_index, construction_test) {

	std::vector<int> values{1, 2, 3, 4};
	veb_index<int, decltype(values)::iterator> index(begin(values), end(values));

	std::vector<node<int, decltype(values)::iterator>> nodes{
	    {std::optional<int>(4), 0, 1}, {std::optional<int>(2), 0, 3},
	    {std::optional<int>(4), 0, 5},

	    {values.begin(), 1},           {values.begin() + 1, 1},

	    {values.begin() + 2, 2},       {values.begin() + 3, 2}};

	const auto data = index.data();

	EXPECT_THAT(data, ContainerEq(nodes));
}

TEST(veb_index, construction_with_gaps_test) {
	std::vector<std::optional<int>> values{std::nullopt, std::nullopt, std::nullopt, 4};
	veb_index<int, decltype(values)::iterator> index(begin(values), end(values));

	std::vector<node<int, decltype(values)::iterator>> nodes{
	    {std::optional<int>(4), 0, 1}, {std::nullopt, 0, 3},
	    {std::optional<int>(4), 0, 5},

	    {values.begin(), 1},           {values.begin() + 1, 1},

	    {values.begin() + 2, 2},       {values.begin() + 3, 2}};

	const auto data = index.data();

	EXPECT_THAT(data, ContainerEq(nodes));
}

TEST(veb_index, find_test) {
	std::vector<int> values{0, 1, 2, 3};
	veb_index<int, decltype(values)::iterator> index(begin(values), end(values));

	for (std::size_t i = 0; i < values.size(); ++i) {
		EXPECT_EQ(index.find(i), std::find(begin(values), end(values), i));
	}
}

TEST(veb_index, find_with_gaps_test) {
	std::vector<std::optional<int>> values{std::nullopt, std::nullopt, std::nullopt, 4};
	veb_index<int, decltype(values)::iterator> index(begin(values), end(values));

	EXPECT_EQ(index.find(5), std::find(begin(values), end(values), 4));
	EXPECT_EQ(index.find(3), std::find(begin(values), end(values), 4));
}

TEST(veb_index, update_test) {
	std::vector<int> values{1, 2, 3, 4};
	veb_index<int, decltype(values)::iterator> index(begin(values), end(values));

	values.back() = 10;
	const auto iter =
	    index.update(begin(values) + 2, end(values), [](auto &&v) { return v >= 3; });
	EXPECT_EQ(*iter, 3);
	EXPECT_EQ(iter, values.begin() + 2);

	std::vector<node<int, decltype(values)::iterator>> expected{{10, 0, 1},
	                                                            {2, 0, 3},
	                                                            {10, 0, 5},

	                                                            {values.begin(), 1},
	                                                            {values.begin() + 1, 1},

	                                                            {values.begin() + 2, 2},
	                                                            {values.begin() + 3, 2}};

	EXPECT_THAT(index.data(), ContainerEq(expected));
}

TEST(veb_index, update_with_gaps_test) {
	std::vector<std::optional<int>> values{4, std::nullopt, std::nullopt, std::nullopt};
	veb_index<int, decltype(values)::iterator> index(begin(values), end(values));

	values.back() = 10;
	const auto iter =
	    index.update(begin(values) + 2, end(values), [](auto &&v) { return v == 10; });
	EXPECT_THAT(iter->value(), 10);
	EXPECT_THAT(iter, values.begin() + 3);

	std::vector<node<int, decltype(values)::iterator>> expected{{10, 0, 1},
	                                                            {4, 0, 3},
	                                                            {10, 0, 5},

	                                                            {values.begin(), 1},
	                                                            {values.begin() + 1, 1},

	                                                            {values.begin() + 2, 2},
	                                                            {values.begin() + 3, 2}};

	EXPECT_THAT(index.data(), ContainerEq(expected));
}

TEST(veb_index, update_nullopt_propagation_test) {
	std::vector<std::optional<int>> values{0, std::nullopt, std::nullopt, 4};

	veb_index<int, decltype(values)::iterator> index(begin(values), end(values));

	values.back() = std::nullopt;
	index.update(begin(values) + 3, end(values), [](auto /*unused*/) { return true; });

	std::vector<node<int, decltype(values)::iterator>> expected{{0, 0, 1},
	                                                            {0, 0, 3},
	                                                            {std::nullopt, 0, 5},

	                                                            {values.begin(), 1},
	                                                            {values.begin() + 1, 1},

	                                                            {values.begin() + 2, 2},
	                                                            {values.begin() + 3, 2}};

	EXPECT_THAT(index.data(), ContainerEq(expected));
}

TEST(veb_index, small_index_test) {
	std::vector<int> values{1, 2};
	veb_index<int, decltype(values)::iterator> index(begin(values), end(values));

	std::vector<node<int, decltype(values)::iterator>> expected{
	    {2, 0, 1},
	    {values.begin(), 0},
	    {values.begin() + 1, 0},
	};

	EXPECT_THAT(index.data(), ContainerEq(expected));
}

TEST(veb_index, larger_index_test) {
	std::vector<std::optional<int>> values{1, std::nullopt, 2, std::nullopt,
	                                       3, std::nullopt, 4, std::nullopt};
	veb_index<int, decltype(values)::iterator> index(begin(values), end(values));

	const node<int, decltype(values)::iterator> root = {4, 0};
	EXPECT_EQ(index.data().front(), root);
}

TEST(veb_index, empty_left_subtree_test) {
	std::vector<std::optional<int>> values{std::nullopt, std::nullopt, 0, 5};
	veb_index<int, decltype(values)::iterator> index(begin(values), end(values));
	const auto iter = index.find(3);
	EXPECT_TRUE(iter >= values.begin() + 2 and iter <= values.begin() + 3);
}

TEST(veb_index, empty_right_subtree_test) {
	std::vector<std::optional<int>> values{0, 3, std::nullopt, std::nullopt};
	veb_index<int, decltype(values)::iterator> index(begin(values), end(values));
	const auto iter = index.find(4);
	EXPECT_TRUE(iter >= values.begin() + 1 and iter <= values.begin() + 3);
}

TEST(veb_index, node_test) {
	std::vector<int> values{1};
	node<int, decltype(values)::iterator> x(begin(values), 0);
	node<int, decltype(values)::iterator> y(std::make_optional(10), 0);
	node<int, decltype(values)::iterator> z(std::nullopt, 0);

	EXPECT_EQ(std::max(x, y), y);
	EXPECT_EQ(std::max(x, z), x);
	EXPECT_EQ(std::max(y, z), y);

	std::vector<std::optional<int>> values_with_gaps{5, std::nullopt};
	node<int, decltype(values_with_gaps)::iterator> a(begin(values_with_gaps), 0);
	node<int, decltype(values_with_gaps)::iterator> b(begin(values_with_gaps) + 1, 0);
	node<int, decltype(values_with_gaps)::iterator> c(std::make_optional(10), 0);
	node<int, decltype(values_with_gaps)::iterator> d(std::nullopt, 0);

	EXPECT_EQ(std::max(a, b), a);
	EXPECT_EQ(std::max(a, c), c);
	EXPECT_EQ(std::max(a, d), a);
	EXPECT_EQ(std::max(b, c), c);
	EXPECT_EQ(std::max(c, d), c);
}
