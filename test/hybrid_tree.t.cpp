#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "hybrid_tree.hpp"

#include <numeric>
#include <random>
#include <set>
#include <vector>

using namespace ::testing;

TEST(hybrid_tree, insertion_test) {
	cov::hybrid_tree<int> tree;
	const auto &[iter1, inserted1] = tree.insert(5);
	EXPECT_TRUE(inserted1);
	EXPECT_EQ(*iter1, 5);

	const auto &[iter2, inserted2] = tree.insert(1);
	EXPECT_TRUE(inserted2);
	EXPECT_EQ(*iter2, 1);

	const auto &[iter3, inserted3] = tree.insert(6);
	EXPECT_TRUE(inserted3);
	EXPECT_EQ(*iter3, 6);

	std::vector<int> inorder;
	for (const auto &node : tree) {
		inorder.push_back(node);
	}

	EXPECT_THAT(inorder, ContainerEq(std::vector<int>{1, 5, 6}));
}

TEST(hybrid_tree, many_insertions_test) {
	cov::hybrid_tree<int> tree;
	for (auto i = 1; i <= 100; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);
	}

	std::vector<int> inorder;
	for (const auto &node : tree) {
		inorder.push_back(node);
	}

	std::vector<int> expected;
	std::generate_n(std::back_inserter(expected), 100,
	                [&]() { return expected.size() + 1; });

	EXPECT_THAT(inorder, ContainerEq(expected));
}

TEST(hybrid_tree, double_insertion_test) {
	cov::hybrid_tree<int> tree;
	tree.insert(1);
	const auto [iter, inserted] = tree.insert(1);

	EXPECT_FALSE(inserted);
	EXPECT_EQ(*iter, 1);
}

TEST(hybrid_tree, iterator_traversal_test) {
	cov::hybrid_tree<int> tree;
	for (auto i = 1; i <= 4; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);
	}

	std::vector<int> inorder;
	for (auto iter = tree.find(2); iter != tree.end(); ++iter) {
		inorder.push_back(*iter);
	}

	EXPECT_THAT(inorder, ContainerEq(std::vector<int>{2, 3, 4}));
}

TEST(hybrid_tree, iterator_mutation_test) {
	cov::hybrid_tree<int> tree;
	for (auto i = 1; i <= 4; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);

		*iter = i - 1;
	}

	std::vector<int> inorder;
	for (const auto &node : tree) {
		inorder.push_back(node);
	}

	EXPECT_THAT(inorder, ContainerEq(std::vector<int>{0, 1, 2, 3}));
}

TEST(hybrid_tree, backwards_iteration_test) {
	cov::hybrid_tree<int> tree;
	for (auto i = 0; i <= 20; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);
	}

	auto iter = tree.find(5);
	std::vector<int> ordered{*iter};

	do {
		--iter;
		ordered.push_back(*iter);
	} while (iter != tree.begin());

	EXPECT_THAT(ordered, ContainerEq(std::vector<int>{5, 4, 3, 2, 1, 0}));
}

TEST(hybrid_tree, erase_test) {
	cov::hybrid_tree<int> tree;
	for (auto i = 0; i <= 10; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);
	}

	auto iter = tree.find(5);
	EXPECT_EQ(*iter, 5);

	auto next_iter = tree.erase(iter);
	EXPECT_EQ(*next_iter, 6);

	std::vector<int> ordered;
	for (const auto &x : tree) {
		ordered.push_back(x);
	}

	std::vector<int> expected(11);
	std::iota(expected.begin(), expected.end(), 0);
	expected.erase(std::find(expected.begin(), expected.end(), 5));

	EXPECT_THAT(ordered, ContainerEq(expected));
}

TEST(hybrid_tree, iterator_swap_test) {
	cov::hybrid_tree<int> tree;

	for (auto i = 0; i < 10; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);
	}

	auto iter1 = tree.find(5);
	auto iter2 = tree.find(6);
	std::swap(iter1, iter2);

	std::vector<int> ordered;
	for (const auto &x : tree) {
		ordered.push_back(x);
	}

	std::vector<int> expected(10);
	std::iota(expected.begin(), expected.end(), 0);

	EXPECT_THAT(ordered, ContainerEq(expected));
}

TEST(hybrid_tree, load_test) {
	cov::hybrid_tree<int> tree;

	const auto insertions = 1500;
	for (auto i = 0; i <= insertions; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);
	}

	for (auto i = 700; i <= 1000; ++i) {
		auto iter = tree.find(i);

		auto next_iter = tree.erase(iter);
		EXPECT_EQ(*next_iter, i + 1);
	}

	std::vector<int> inorder;
	for (const auto &v : tree) {
		inorder.push_back(v);
	}

	std::vector<int> expected(700);
	std::iota(expected.begin(), expected.end(), 0);

	std::vector<int> upper_half(500);
	std::iota(upper_half.begin(), upper_half.end(), 1001);

	expected.insert(expected.end(), upper_half.begin(), upper_half.end());
	EXPECT_THAT(inorder, ContainerEq(expected));
}

TEST(hybrid_tree, reverse_insertion_test) {
	cov::hybrid_tree<int> tree;

	const auto insertions = 500;
	for (auto i = insertions; i > 0; --i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);
	}

	std::vector<int> inorder;
	for (const auto &v : tree) {
		inorder.push_back(v);
	}

	std::vector<int> expected(insertions);
	std::iota(expected.begin(), expected.end(), 1);

	EXPECT_THAT(inorder, ContainerEq(expected));
}

TEST(hybrid_tree, random_insertion_test) {
	const uint64_t seed = 2212383;
	std::mt19937 rng(seed);
	std::uniform_int_distribution<int> dist{};

	const auto elements = 500;
	cov::hybrid_tree<int> hybrid_tree;
	std::set<int> std_set;

	for (auto i = 0; i < elements; ++i) {
		const auto v = dist(rng);
		std_set.insert(v);
		const auto [iter, inserted] = hybrid_tree.insert(v);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, v);
	}

	std::vector<int> inorder(hybrid_tree.begin(), hybrid_tree.end());
	std::vector<int> expected(std_set.begin(), std_set.end());

	EXPECT_THAT(inorder, ContainerEq(expected));
}

TEST(hybrid_tree, index_update_test) {
	cov::hybrid_tree<int> tree;
	for (int i = 1; i <= 67; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);
	}

	for (int i = 1; i <= 67; ++i) {
		const auto iter = tree.find(i);
		EXPECT_EQ(*iter, i);
	}
}

TEST(hybrid_tree, erase_regression) {
	cov::hybrid_tree<int> values;
	constexpr auto elements = 10000;

	for (int val = 0; val < elements; ++val) {
		values.insert(val);
	}

	for (int val = 0; val < elements; ++val) {
		const auto iter = values.find(val);
		values.erase(iter);
	}
}
