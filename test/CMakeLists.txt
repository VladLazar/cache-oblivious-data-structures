add_subdirectory(lib)
include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})
include_directories(${gmock_SOURCE_DIR}/include ${gmock_SOURCE_DIR})

set(TESTS
    cov_tree.t.cpp
    hybrid_tree.t.cpp
    small_height_tree.t.cpp
    static_veb_tree.t.cpp
    static_bfs_tree.t.cpp
    packed_memory_array.t.cpp
    veb_index.t.cpp
    bfs_index.t.cpp
    TreeMeta.t.cpp)

add_executable(cov_lib_tests ${TESTS})

if(CMAKE_BUILD_TYPE MATCHES Debug)
    message("Enabling sanitizers ..")
    enable_sanitizers(cov_lib_tests)
endif()

target_compile_options(cov_lib_tests INTERFACE "${COV_CXX_FLAGS}")

target_link_libraries(cov_lib_tests cov_lib)
target_link_libraries(cov_lib_tests gtest gtest_main gmock gmock_main)

add_test(tests cov_lib_tests)

