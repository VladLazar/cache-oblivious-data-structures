#include <gmock/gmock-matchers.h>
#include <gmock/gmock-more-matchers.h>

#include <algorithm>
#include <iterator>
#include <numeric>
#include <optional>

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "packed_memory_array.hpp"

using namespace ::testing;

template <typename T>
std::vector<std::optional<T>> filter_null(const std::vector<std::optional<T>> &values) {
	std::vector<std::optional<T>> filtered;
	std::copy_if(begin(values), end(values), std::back_inserter(filtered),
	             [](auto v) { return v.has_value(); });

	return filtered;
}

TEST(packed_memory_array, range_constructor_test) {
	std::vector<int> values{1, 2, 3, 4};
	cov::packed_memory_array<int> pma(values.begin(), values.end());

	EXPECT_EQ(pma.size(), values.size());
	EXPECT_EQ(pma.capacity(), std::pow(2, 3));

	EXPECT_THAT(pma.data(), IsSupersetOf(values));

	auto data = filter_null(pma.data());
	EXPECT_TRUE(std::is_sorted(begin(data), end(data)));
}

TEST(packed_memory_array, insertion_test) {
	cov::packed_memory_array<int> pma;

	pma.insert(0, 3);
	pma.insert(1, 5);

	EXPECT_EQ(pma.size(), 2);
	EXPECT_EQ(pma.capacity(), 4);

	EXPECT_THAT(pma.data(), ContainerEq(std::vector<std::optional<int>>{
	                            3, std::nullopt, 5, std::nullopt}));
}

TEST(packed_memory_array, hammer_insertion_test) {
	cov::packed_memory_array_config config;
	cov::packed_memory_array<int> pma(config);

	const auto insertions = 1000;
	for (auto i = 0; i < insertions; ++i) {
		pma.insert(0, i);
	}

	EXPECT_EQ(pma.size(), insertions);

	const auto capacity = std::pow(2, std::ceil(std::log2(insertions / config.tau_root)));
	EXPECT_EQ(pma.capacity(), capacity);

	std::vector<std::optional<int>> expected_data(insertions);
	std::iota(begin(expected_data), end(expected_data), 0);

	auto data = filter_null(pma.data());
	EXPECT_THAT(data, UnorderedElementsAreArray(expected_data));
}

TEST(packed_memory_array, returned_range_trivial_test) {
	cov::packed_memory_array<int> pma;

	{
		const auto [first, last] = pma.insert(0, 1);

		EXPECT_EQ(first + 1, last);
		EXPECT_EQ(std::distance<std::vector<std::optional<int>>::const_iterator>(
		              begin(pma.data()), first),
		          0);
	}

	const auto [first, last] = pma.insert(0, 2);
	EXPECT_EQ(first, begin(pma.data()));
	EXPECT_EQ(last, end(pma.data()));
}

TEST(packed_memory_array, returned_range_test) {
	cov::packed_memory_array<int> pma;

	const auto iterations = 100;
	for (int i = 0; i < iterations; ++i) {
		const auto &old_data = pma.data();
		const auto [left, right] = pma.insert(0, i);

		/* If the insert did not result in a grow check that
		 * nothing outside the range was changed. */
		if (pma.capacity() == old_data.size()) {
			const auto mismatch =
			    std::mismatch(begin(old_data), end(old_data), begin(pma.data()));

			EXPECT_TRUE(mismatch.second >= left);

			const auto old_right =
			    begin(old_data) +
			    std::distance<std::vector<std::optional<int>>::const_iterator>(
			        begin(pma.data()), right);

			const auto equal_after_range =
			    std::equal<std::vector<std::optional<int>>::const_iterator>(
			        right, end(pma.data()), old_right);

			EXPECT_TRUE(equal_after_range);
		} else { /* Otherwise, check that the entire range was returned. */
			EXPECT_EQ(pma.capacity(), old_data.size() * 2);
			EXPECT_EQ(left, begin(pma.data()));
			EXPECT_EQ(right, end(pma.data()));
		}
	}
}

TEST(packed_memory_array, growth_test) {
	cov::packed_memory_array<int> pma;

	pma.insert(0, 3);
	pma.insert(1, 5);

	EXPECT_EQ(pma.size(), 2);
	EXPECT_EQ(pma.capacity(), 4);

	EXPECT_THAT(pma.data(), IsSupersetOf({3, 5}));
}

TEST(packed_memory_array, removal_test) {
	cov::packed_memory_array<int> pma;

	pma.insert(0, 1);

	EXPECT_EQ(pma.size(), 1);
	EXPECT_THAT(pma.data(), IsSupersetOf({1}));

	pma.remove(0);

	EXPECT_EQ(pma.size(), 0);
	EXPECT_THAT(filter_null(pma.data()), IsEmpty());
}

TEST(packed_memory_array, shrink_test) {
	std::vector<int> values{1, 2, 3, 4};
	cov::packed_memory_array<int> pma(begin(values), end(values));

	EXPECT_EQ(pma.size(), 4);
	EXPECT_EQ(pma.capacity(), 8);

	pma.remove(std::distance(pma.cbegin(), pma.find(1)));
	pma.remove(std::distance(pma.cbegin(), pma.find(2)));

	EXPECT_EQ(pma.size(), 2);
	EXPECT_EQ(pma.capacity(), 4);

	EXPECT_THAT(pma.data(), IsSupersetOf({4}));
}
