#include "static_veb_tree.hpp"

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <algorithm>
#include <array>
#include <numeric>

using namespace ::testing;

TEST(static_veb_tree, default_constructtion_test) {
	cov::static_veb_tree<int> def;

	EXPECT_EQ(def.size(), 0);
}

TEST(static_veb_tree, construction_test) {
	std::array<int, 6> values = {1, 2, 3, 4, 5, 6};
	std::array<int, 6> indices = {3, 1, 4, 0, 5, 2};

	cov::static_veb_tree<int> tree(values.begin(), values.end());
	EXPECT_EQ(tree.size(), 6);

	std::size_t i = 0;
	for (const auto &x : values) {
		const auto index = tree.find(x);

		EXPECT_TRUE(index != cov::static_veb_tree<int>::npos);
		EXPECT_EQ(index, indices.at(i++));
	}
}

TEST(static_veb_tree, large_construction_test) {
	std::array<int, 10000> values;
	std::iota(values.begin(), values.end(), 1);

	cov::static_veb_tree<int> tree(values.begin(), values.end());
	EXPECT_EQ(tree.size(), 10000);
}

TEST(static_veb_tree, stress_test) {
	std::array<int, 5000> values;
	std::iota(values.begin(), values.end(), 0);

	cov::static_veb_tree<int> tree(values.begin(), values.end());

	for (const auto &x : values) {
		const auto index = tree.find(x);
		EXPECT_TRUE(index != cov::static_veb_tree<int>::npos);
	}

	EXPECT_EQ(tree.find(-1), cov::static_veb_tree<int>::npos);
	EXPECT_EQ(tree.find(5005), cov::static_veb_tree<int>::npos);
}
